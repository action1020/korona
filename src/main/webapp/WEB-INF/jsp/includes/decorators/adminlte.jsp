<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <sec:csrfMetaTags/>
    <title><decorator:title default="DaRiDari"/></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/dist/css/skins/_all-skins.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!--  CDC style -->
    <link rel="stylesheet" href="${ globalCtx }/public/e2on/css/cdc.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!--  AX5 -->
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5grid.css">
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5calendar.css">
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5picker.css">
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5toast.css">

    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5select.css">
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5combobox.css">

    <!-- jQuery 3 -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- jvectormap -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- daterangepicker -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/moment/min/moment.min.js"></script>
    <!-- Slimscroll -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/dist/js/adminlte.min.js"></script>

    <!-- bootbox -->
    <script src="${ globalCtx }/public/e2on/js/bootbox.js"></script>
    <!-- Validator -->
    <script src="${ globalCtx }/public/e2on/js/validator.min.js"></script>
    <!-- AX5 -->
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5core.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5grid.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5calendar.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5picker.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5binder.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5toast.min.js"></script>

    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5select.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5combobox.min.js"></script>

    <!-- Web Console Apps -->
    <script src="${ globalCtx }/public/js/apps.js"></script>

    <!-- bookmark -->
    <style type="text/css">
        .control-sidebar {
            top: 0;
            right: -420px;
            width: 420px;
            min-height: 100%;
            -webkit-transition: right .3s ease-in-out;
            -o-transition: right .3s ease-in-out;
            transition: right .3s ease-in-out;
        }
    </style>

    <decorator:head/>
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Dari</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>DariDari</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><i
                                class="fa fa-user"></i> ${ sessionScope.WEB_SESSION.name } ( ${ sessionScope.WEB_SESSION.loginId } ) 님 </span>
                    </a>
                    <ul class="dropdown-menu"
                        style="border-top-right-radius: 0;border-top-left-radius: 0;padding: 1px 0 0 0;border-top-width: 0;width: 400px">

                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-12 text-center" style="margin-top:5px;margin-bottom:15px">
                                    <p>${ sessionScope.WEB_SESSION.name } 님의 비밀번호 변경</p>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:20px">
                                <form name="passForm" id="passForm" onsubmit="return false;">
                                    <div class="col-xs-4 text-center">
                                        <label>신규 비밀번호</label>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <input type="password" name="password" class="form-control"
                                               placeholder="신규 비밀번호"/>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <label>비밀번호 확인</label>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <input type="password" name="password2" class="form-control" placeholder="비밀번호 확인"/>
                                </div>
                            </div>
                            </form>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="#password" class="btn btn-default btn-flat"
                                   onclick="cdc.data.passwordChange('${ globalCtx }');">비밀번호 변경</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="${ globalCtx }/logout"><i class="fa fa-sign-out"></i> Logout</a>
                </li>

            </ul>
        </div>
    </nav>
</header>

<%@include file="adminlte_lnb.jsp" %>

<decorator:body/>

<div style="position:absolute; top:45%; left:50%;display:none;" id="GlobalLoading"><span
        style="font-weight:bold;font-size:50px;"><i class="fa fa-spinner fa-spin " aria-hidden="true"></i></span></div>

<footer class="main-footer">
    <strong>Copyright Gold</strong> All rightsreserved.
</footer>

</body>
</html>