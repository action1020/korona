<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <sec:authorize access="hasAnyRole('ROLE_ROOT', 'ROLE_USER')">
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-file-text-o"></i> <span>수집</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="${ globalCtx }/collector/collectorList"><i class="fa fa-pie-chart"></i><span>예상결과</span></a></li>
                    </ul>
                </li>
            </sec:authorize>

            <sec:authorize access="hasAnyRole('ROLE_ROOT')">
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i> <span>설정</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="${ globalCtx }/manager/retrieveUserList"><i class="fa fa-user-plus"></i> <span>사용자관리</span></a></li>
                    </ul>
                </li>
            </sec:authorize>

        </ul>
        <!-- // sidebar menu: : style can be found in sidebar.less -->
    </section>
    <!-- /.sidebar -->
</aside>

