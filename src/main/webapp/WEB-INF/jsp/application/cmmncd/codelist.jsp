<%--
  User: action1020s@gmail.com
  Date: 2018-01-19
  Time: 오후 2:26
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>공통코드 관리</title>
    <meta name="decorator" content="adminlte"/>
    <!-- AX5 -->
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/ax5ui/ax5grid.css">

    <script type="text/javascript" src="${ globalCtx }/public/ax5ui/ax5core.min.js"></script>
    <script type="text/javascript" src="${ globalCtx }/public/ax5ui/ax5grid.min.js"></script>
    <style type="text/css">
        .filter-col {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>

</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Tables
            <small>advanced tables</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form class="form-inline" name="searchForm" id="searchForm">
                                    <!-- HIDDEN FIELD[S] -->
                                    <input type="hidden" name="pageNo" value="0"/>
                                    <input type="hidden" name="pageSize" value="500"/>
                                    <!-- HIDDEN FIELD[E] -->

                                    <div class="form-group">
                                        <label class="filter-col" style="margin-right:0;"
                                               for="pref-search">Search:</label>
                                        <select class="form-control" name="searchOption">
                                            <option>All</option>
                                            <option>Code</option>
                                            <option>Title</option>
                                            <option>Code+Title</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="filter-col" style="margin-right:0;"
                                               for="pref-search">Keyword:</label>
                                        <input type="text" class="form-control" name="searchWord" placeholder="Keyword">
                                        <a href="#search" class="btn btn-sm btn-success" role="button"
                                           onclick="Apps.grid.search();"><i class="fa fa-search" aria-hidden="true"></i>
                                            Search</a>
                                    </div>

                                    <div class="form-group pull-right">
                                        <a href="#add" class="btn btn-sm btn-primary" role="button"><i
                                                class="fa fa-plus-square" aria-hidden="true"></i> Add</a>
                                        <a href="#mod" class="btn btn-sm btn-info" role="button"><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i> Modify</a>
                                        <a href="#del" class="btn btn-sm btn-danger" role="button"><i
                                                class="fa  fa-plus-square" aria-hidden="true"></i> Delete</a>
                                    </div>

                                </form>
                            </div>
                        </div>

                        <div data-ax5grid="first-grid" data-ax5grid-config="{}" style="height: 500px;"></div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Recipient:</label>
                            <input type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Message:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->

<script type="text/javascript">

    var Apps = {
        start: function () {

            Apps.grid.init();

        },
        grid: {
            target: new ax5.ui.grid(),
            init: function () {

                Apps.grid.target.setConfig({
                    target: $('[data-ax5grid="first-grid"]'),
                    sortable: true, // 모든 컬럼에 정렬 아이콘 표시
                    multiSort: false,// 다중 정렬 여부
                    columns: [
                        {key: "rasc", label: "rasc"},
                        {key: "rdesc", label: "rdesc"},
                        {key: "totalcount", label: "totalcount"},
                        {key: "dateDimId", label: "dateDimId"},
                        {key: "dateActual", label: "dateActual"}
                    ],
                    body: {
                        align: "center",
                        columnHeight: 28,
                        onClick: function () {

                            this.self.select(this.dindex, {selectedClear: true});
                            console.log(this.item);
                        }
                    },
                    page: {
                        navigationItemCount: 9,
                        height: 30,
                        display: true,
                        firstIcon: '<i class="fa fa-step-backward" aria-hidden="true"></i>',
                        prevIcon: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
                        nextIcon: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                        lastIcon: '<i class="fa fa-step-forward" aria-hidden="true"></i>',
                        onChange: function () {
                            Apps.grid.search(this.page.selectPage + 1);
                        }
                    }
                });

            },
            search: function (pageNo) {

                $("#searchForm>input[name=pageNo]").val((pageNo - 1) || 0);

                $.ajax({
                    url: "${ globalCtx }/code/retrieveCodeList.json",
                    method: "post",
                    data: $("#searchForm").serialize()
                }).done(function (ds) {

                    if ("ok" == ds.result) {
                        Apps.grid.target.setData(ds);
                    }

                });
            }
        }
    };

</script>
</body>
</html>
