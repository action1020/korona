<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>예상결과</title>
    <meta name="decorator" content="admin"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-pie-chart"></i> 예상결과</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form name="searchForm" id="searchForm" onsubmit="Apps.grid.search(); return false;" class="form-horizontal">
                                    <!-- HIDDEN FIELD[S] -->
                                    <input type="hidden" name="pageNo" value="0"/>
                                    <input type="hidden" name="pageSize" value="500"/>
                                    <!-- HIDDEN FIELD[E] -->

                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">게임</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="searchOption" onchange="Apps.grid.search();">
                                                <option value="">전체</option>
                                                <option value="Bubble2_1m">1분 버블버블사다리</option>
                                                <option value="Mario1m">1분 마리오</option>
                                                <option value="Mario2m">2분 마리오</option>
                                                <option value="Mario3m">3분 마리오</option>
                                                <%--<option value="Bubble1m">1분 뽀글이</option>
                                                <option value="Bubble2m">2분 뽀글이</option>
                                                <option value="Bubble3m">3분 뽀글이</option>--%>
                                                <option value="Star1m">1분 별다리</option>
                                                <option value="Star2m">2분 별다리</option>
                                                <option value="Star3m">3분 별다리</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="pull-right">
                                        <select class="form-control" id="time">
                                            <option value="40">40초</option>
                                            <option value="50">50초</option>
                                            <option value="60">1분</option>
                                            <option value="110">1분40초</option>
                                            <option value="120">2분</option>
                                            <option value="160">2분40초</option>
                                            <option value="180">3분</option>
                                        </select>
                                        <p id="timeinfo" style="font-bold:bold;color:blue;"></p>
                                        <a href="#search" class="btn btn-sm btn-warning" role="button" onclick="Apps.grid.create();"><i class="fa fa-search" aria-hidden="true"></i>바로생성</a>
                                        <a href="#search" class="btn btn-sm btn-success" role="button" onclick="Apps.grid.auto(this);"><i class="fa fa-search" aria-hidden="true"></i>자동</a>
                                        <a href="#search" class="btn btn-sm btn-success" role="button" onclick="Apps.grid.search();"><i class="fa fa-search" aria-hidden="true"></i>조회</a>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div>
                            <p id="summary"></p>
                        </div>

                        <div data-ax5grid="first-grid" data-ax5grid-config="{}" style="height: 500px;"></div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();
            Apps.data.init();
            Apps.grid.init();

        },
        init: function () {

            // event binding for searchForm elements
            $('#searchForm > *').bind('keypress', function (e) {

                if ("13" == e.keyCode) {
                    if (e) e.preventDefault();
                    Apps.grid.search();
                }
            });

        },
        grid: {
            target: new ax5.ui.grid(),
            init: function () {

                Apps.grid.target.setConfig({
                    target: $('[data-ax5grid="first-grid"]'),
                    columns: [
                        {key: "rasc", label: "순번"},
                        {key: "name", label: "게임", sortable: true, align: "left", width: 200},
                        {key: "regdt", label: "일자", sortable: true, width: 130},
                        {key: "round", label: "회차", sortable: true, align: "left", width: 100},
                        {
                            key: "", label: "예상", align: "center", width: 50, formatter: function () {
                                return this.item.odd ? "홀":"짝";
                            }
                        },
                        {
                            key: "", label: "적중", sortable: true, width: 50, formatter: function () {
                                return this.item.status == 2 ? "대기": this.item.status == 1 ? "<span style='background:blue; color: white; font-size:12px'>적중</span>":"<span style='background:red;color:white; font-size:12px'>미적</span>";
                            }
                        },
                        {
                            key: "", label: "출발", sortable: true, width: 50, formatter: function () {
                                return this.item.start ? "좌":"우";
                            }
                        },
                        {
                            key: "", label: "3/4", sortable: true, width: 50, formatter: function () {
                                return this.item.line ? "3":"4";
                            }
                        },
                        {key: "remark", label: "비고", sortable: true, align: "left", width: 400},
                    ],
                    header: {
                        align: "center",
                        columnHeight: 50
                    },
                    body: {
                        align: "center",
                        columnHeight: 28,
                        onClick: function () {

                            if (this.colIndex == 10) {
                                Apps.data.execute(this.item);
                            }

                            if (this.colIndex == 11) {
                                Apps.data.bind(this.item);
                            }

                        },
                        onDBLClick: function () {

                            // active via toggle
                            if (this.colIndex == 9) {
                                this.item.active = this.item.active ? false : true;
                                Apps.data.toggle(this.item);
                            }

                        }
                    },
                    page: {
                        navigationItemCount: 9,
                        height: 30,
                        display: true,
                        firstIcon: '<i class="fa fa-step-backward" aria-hidden="true"></i>',
                        prevIcon: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
                        nextIcon: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                        lastIcon: '<i class="fa fa-step-forward" aria-hidden="true"></i>',
                        onChange: function () {
                            Apps.grid.search(this.page.selectPage + 1);
                        }
                    }
                });

                Apps.grid.search();
            },
            polling : null,
            time: null,
            timeinfo : function(){

                if( Apps.grid.time == null ){
                    Apps.grid.time = $("#time").val();
                }
                Apps.grid.time = Apps.grid.time - 1;
                $("#timeinfo").text(Apps.grid.time + " 초후 새로고침");

                if ( Apps.grid.time == 0 ){
                    Apps.grid.time = null;
                    Apps.grid.search();
                    Apps.grid.time = $("#time").val();
                }
            },

            auto : function(obj){

                if ( Apps.grid.polling == null ){

                    $(obj).removeClass('btn-success').addClass('btn-primary');
                    Apps.grid.polling = setInterval(function() {
                        Apps.grid.timeinfo();
                    }, 1000);

                }else{

                    $(obj).removeClass('btn-primary').addClass('btn-success');
                    Apps.grid.polling = null;
                    Apps.grid.time = null;
                    clearInterval(Apps.grid.polling);

                }

            },
            create : function(){

                var params = $("#searchForm").serialize();
                var url = "${ globalCtx }/collector/rest/createTips";

                $.ajax({
                    url: url,
                    data: params,
                    type: "json",
                    method: "post"
                }).done(function (res) {

                        if (cdc.code.ok == res.result) {
                            Apps.grid.search();
                        }
                });
                /**/

            },
            search: function (pageNo) {

                $("#searchForm>input[name=pageNo]").val((pageNo - 1) || 0);

                var params = $("#searchForm").serialize();
                var url = "${ globalCtx }/collector/rest/retrieveCollectorList";

                /**/
                $.ajax({
                    url: url,
                    data: params,
                    type: "json",
                    method: "post"
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {
                        Apps.grid.target.setData(res);

                        var url = "${ globalCtx }/collector/rest/getSummary";

                        /**/
                        $.ajax({
                            url: url,
                            data: params,
                            type: "json",
                            method: "post"
                        }).done(function (res) {

                            if (cdc.code.ok == res.result) {
                                // 현재 게임수 : 100 개 , 적중 : 30건, 미적 20건, 적중률 ( 100% )

                                var tot = 0, success = 0, fail = 0, percent = 0;
                                $.each(res.list, function(i){

                                    if ( res.list[i].status == 0 ) {
                                        fail = res.list[i].count;
                                    }
                                    if ( res.list[i].status == 1 ){
                                        success = res.list[i].count;
                                    }
                                });

                                tot = success + fail;
                                percent = ((success/tot) * 100).toFixed(2);

                                $("#summary").html("<i class=\"fa fa-line-chart\"></i>현재 게임수 : " + tot + " 개 , 적중 : " + success  + "건, 미적 " + fail + "건, 적중률 ( " + percent +"% )");


                            } else {
                                cdc.toast.push(res.message, {theme: "danger"});
                            }

                        });

                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });
                /**/

            }
        },

        modal: {
            open: function () {
                Apps.data.target.setModel({}, $("#saveForm"));
                $("#modal1").modal('show');
            },
            close: function () {
                $("#modal1").modal('hide');
            }
        },

        data: {
            target: new ax5.ui.binder(),
            ds: null,
            init: function () {


                $('#saveForm').validator('update');


            },

            bind: function (obj) {

                Apps.data.target.setModel(obj, $("#saveForm"));

                $("#modal1").modal('show');
                $('#saveForm').validator('validate');
            },
            save: function () {

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 저장하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 저장'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            var url = "${ globalCtx }/collector/rest/createOrUpdate";
                            var params = $.param(Apps.data.target.get());

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {

                                    cdc.toast.push("완료되었습니다.");
                                    Apps.modal.close();
                                    Apps.grid.search();

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });

                        }

                    }

                });

            },
            toggle: function (item) {

                var url = "${ globalCtx }/collector/rest/createOrUpdate";
                var params = $.param(item);

                $.ajax({
                    url: url,
                    data: params,
                    type: 'json',
                    method: 'post'
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {

                        cdc.toast.push("완료되었습니다.");
                        Apps.grid.search();

                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });

            },
            execute: function (item) {

                if (item.clctStatCd != 10) {
                    cdc.toast.push(item.clctStatCdNm, {theme: "danger"});
                    return;
                }

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 실행하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 실행'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            /**/
                            var url = "${ globalCtx }/collector/rest/collectorStart";
                            var params = $.param(item);

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {

                                    cdc.toast.push("수집 요청이 완료되었습니다.");
                                    Apps.modal.close();
                                    Apps.grid.search();

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });
                            /**/

                        }

                    }

                });
            }
        }
    };
</script>
<!-- / content-script -->
</body>
</html>
