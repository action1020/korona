<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>수집목록</title>
    <meta name="decorator" content="admin"/>
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5autocomplete.css">
    <script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5autocomplete.min.js"></script>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-file-text-o"></i> 수집목록</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form name="searchForm" id="searchForm" onsubmit="Apps.grid.search(); return false;"
                                      class="form-horizontal">
                                    <!-- HIDDEN FIELD[S] -->
                                    <input type="hidden" name="pageNo" value="0"/>
                                    <input type="hidden" name="pageSize" value="500"/>
                                    <!-- HIDDEN FIELD[E] -->

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">수집일자</label>
                                        <div class="col-sm-2">

                                            <div class="input-group" data-ax5picker="search1">
                                                <input type="text" name="searchStart" id="searchStart"
                                                       class="form-control" placeholder="yyyy-mm-dd"
                                                       readonly="readonly">
                                                <span class="input-group-addon">~</span>
                                                <input type="text" name="searchEnd" id="searchEnd" class="form-control"
                                                       placeholder="yyyy-mm-dd" readonly="readonly">
                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">채널</label>
                                        <div class="col-sm-11">

                                            <c:forEach var="item" items="${ channelCdCodeList }" varStatus="status">
                                                <label style="margin-right:15px"><input type="checkbox"
                                                                                        name="searchIntegerArr"
                                                                                        value="${ item.cdVal }"> ${ item.cdNm }
                                                </label>
                                            </c:forEach>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">분류 키워드</label>
                                        <div class="col-sm-10">
                                            <div id="searchOption2" data-ax5autocomplete="searchIntegerArr2"
                                                 data-ax5autocomplete-config="{multiple: true}"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">검색어</label>
                                        <div class="col-sm-11">
                                            <input type="text" class="form-control" name="searchWord" placeholder="검색어">

                                        </div>
                                    </div>

                                    <div class="pull-right">
                                        <a href="#search" class="btn btn-sm btn-success" role="button"
                                           onclick="Apps.grid.search();"><i class="fa fa-search" aria-hidden="true"></i>
                                            검색</a>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <div data-ax5grid="first-grid" data-ax5grid-config="{}" style="height: 500px;"></div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

    <!-- Attachment -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" data-backdrop="static"
         data-keyboard="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal1_title"></h4>
                </div>

                <div class="modal-body">
                    <div data-ax5grid="file-grid" data-ax5grid-config="{}" style="height: 500px;"></div>
                </div>

            </div>
        </div>
    </div>

    <!-- Link -->
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" data-backdrop="static"
         data-keyboard="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal2_title"></h4>
                </div>

                <div class="modal-body">
                    <div data-ax5grid="link-grid" data-ax5grid-config="{}" style="height: 500px;"></div>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();
            Apps.picker.init();
            Apps.grid.init();
            Apps.auto.init();
            Apps.data.attachment.grid.init();
            Apps.data.link.grid.init();
        },
        init: function () {

            // event binding for searchForm elements
            $('#searchForm > *').bind('keypress', function (e) {

                if ("13" == e.keyCode) {
                    if (e) e.preventDefault();
                    Apps.grid.search();
                }
            });

        },
        grid: {
            target: new ax5.ui.grid(),
            init: function () {

                Apps.grid.target.setConfig({
                    target: $('[data-ax5grid="first-grid"]'),
                    columns: [
                        {key: "rasc", label: "순번"},
                        {key: "channelCdNm", label: "채널", sortable: true, align: "left", width: 150},
                        {key: "kwdNames", label: "분류 키워드", sortable: true, align: "left", width: 150},
                        {key: "title", label: "제목", sortable: true, align: "left", width: 450},
                        {key: "summaryCntnt", label: "요약글", sortable: true, align: "left", width: 350},
                        {
                            key: "", label: "PDF", align: "center", width: 50, formatter: function () {
                                return this.item.cntntlength > 0 ? "<a href='${ globalCtx }/collector/retrievePdfFile?seq=" + this.item.articleSeq + "' target='_blank'><i class='fa fa-file-pdf-o fa-lg'></i></a>" : "";
                            }
                        },
                        {
                            key: "attachCnt", label: "첨부파일", width: 80, formatter: function () {
                                return this.item.attachCnt > 0 ? "<a href='#download'><i class='fa fa-files-o fa-lg'></i></a>" : "";
                            }
                        },
                        {
                            key: "linkCnt", label: "링크정보", width: 80, formatter: function () {
                                return this.item.linkCnt > 0 ? "<a href='#link'><i class='fa fa-hand-o-up fa-lg'></i></a>" : "";
                            }
                        },
                        {
                            key: "srcUrl", label: "원본링크", width: 80, formatter: function () {
                                return "<a href='" + this.item.srcUrl + "' target='_blank'><i class='fa fa-external-link fa-lg'></i></a>";
                            }
                        },
                        {key: "writeDtm", label: "작성일자", sortable: true, width: 120},
                        {key: "clctDtm", label: "수집일자", sortable: true, width: 120}
                    ],
                    header: {
                        align: "center",
                        columnHeight: 50
                    },
                    body: {
                        align: "center",
                        columnHeight: 28,
                        onClick: function () {

                            if (this.colIndex == 6) {
                                Apps.data.attachment.load(this.item);
                            }

                            if (this.colIndex == 7) {
                                Apps.data.link.load(this.item);
                            }

                        },
                        onDBLClick: function () {

                            // active via toggle
                            if (this.colIndex == 9) {
                                this.item.active = this.item.active ? false : true;
                                Apps.data.toggle(this.item);
                            }

                        }
                    },
                    page: {
                        navigationItemCount: 9,
                        height: 30,
                        display: true,
                        firstIcon: '<i class="fa fa-step-backward" aria-hidden="true"></i>',
                        prevIcon: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
                        nextIcon: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                        lastIcon: '<i class="fa fa-step-forward" aria-hidden="true"></i>',
                        onChange: function () {
                            Apps.grid.search(this.page.selectPage + 1);
                        }
                    }
                });

                Apps.grid.search();

            },
            search: function (pageNo) {

                $("#searchForm>input[name=pageNo]").val((pageNo - 1) || 0);

                var params = $("#searchForm").serialize();
                var url = "${ globalCtx }/collector/rest/retriveCollectorDtaList";

                /**/
                $.ajax({
                    url: url,
                    data: params,
                    type: "json",
                    method: "post"
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {
                        Apps.grid.target.setData(res);
                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });
                /**/

            }
        },
        picker: {
            target: new ax5.ui.picker(),
            init: function () {

                Apps.picker.target.bind({
                    target: $('[data-ax5picker="search1"]'),
                    direction: "top",
                    content: {
                        width: 270,
                        margin: 10,
                        type: 'date',
                        config: {
                            control: {
                                left: '<i class="fa fa-chevron-left"></i>',
                                yearTmpl: '%s',
                                monthTmpl: '%s',
                                right: '<i class="fa fa-chevron-right"></i>'
                            },
                            lang: {
                                yearTmpl: "%s",
                                months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                                dayTmpl: "%s"
                            }
                        }
                    }
                });

                $('#searchStart').val(cdc.date.getWeekStartEndDate()[0]);
                $('#searchEnd').val(cdc.date.getWeekStartEndDate()[1]);
            }
        },
        auto: {
            init: function () {

                var options = [];

                $("#searchOption2").ax5autocomplete({
                    removeIcon: '<i class="fa fa-times" aria-hidden="true"></i>',
                    onSearch: function (callBack) {
                        var searchWord = $.trim(this.searchWord);

                        if (searchWord.length > 1) {

                            var params = $.param({searchWord: searchWord});
                            var url = "${ globalCtx }/dictionary/rest/retrieveKwdForAuto";

                            /**/
                            $.ajax({
                                url: url,
                                data: params,
                                type: "json",
                                method: "post"
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {

                                    options = [];
                                    $.each(res.list, function () {
                                        options.push({value: this.value, text: this.text});
                                    })

                                    var regExp = new RegExp(searchWord);
                                    var myOptions = [];

                                    options.forEach(function (n) {
                                        if (n.text.match(regExp)) {
                                            myOptions.push({
                                                value: n.value,
                                                text: n.text
                                            })
                                        }
                                    });
                                    callBack({
                                        options: myOptions
                                    });

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });

                        }
                    }
                });

            }
        },
        data: {
            attachment: {
                load: function (item) {

                    if (item.attachCnt == undefined) {
                        return;
                    }

                    $('#modal1_title').html(item.title);
                    $("#modal1").modal('show');

                    var isLoad = false;
                    $("#modal1").on('shown.bs.modal', function (e) {
                        if (isLoad == false) {
                            Apps.data.attachment.grid.search(item.articleSeq);
                            isLoad = true;
                        }
                    });

                    $("#modal1").on('hidden.bs.modal', function (e) {
                        Apps.grid.target.focus(item.__index);
                    });

                },
                grid: {
                    target: new ax5.ui.grid(),
                    init: function () {

                        Apps.data.attachment.grid.target.setConfig({
                            target: $('[data-ax5grid="file-grid"]'),
                            columns: [
                                {key: "fileNm", label: "첨부 파일", sortable: true, align: "left", width: 350},
                                {key: "srcFileUrl", label: "파일 원본경로", sortable: true, align: "left", width: 250},
                                {
                                    key: "fileSize",
                                    label: "파일크기",
                                    sortable: true,
                                    align: "right",
                                    width: 100,
                                    formatter: function () {
                                        return cdc.data.getFileSize(this.item.fileSize);
                                    }
                                },
                                {
                                    key: "fileSize", label: "다운로드", sortable: true, width: 80, formatter: function () {
                                        return this.item.fileSize > 0 ? "<a href='${ globalCtx }/collector/retrieveFile?seq=" + this.item.attachSeq + "' target='_blank'><i class='fa fa-files-o fa-lg'></i></a>" : "";
                                    }
                                }
                            ],
                            header: {
                                align: "center",
                                columnHeight: 50
                            },
                            body: {
                                align: "center",
                                columnHeight: 28,
                                onClick: function () {
                                }
                            },
                            page: {
                                display: false
                            }
                        });
                    },
                    search: function (articleSeq) {

                        var params = $.param({seq: articleSeq})
                        var url = "${ globalCtx }/collector/rest/retriveCollectorDtaAttachList";

                        /**/
                        $.ajax({
                            url: url,
                            data: params,
                            type: "json",
                            method: "post"
                        }).done(function (res) {

                            if (cdc.code.ok == res.result) {
                                Apps.data.attachment.grid.target.setData(res.list);
                            } else {
                                cdc.toast.push(res.message, {theme: "danger"});
                            }

                        });
                        /**/

                    }
                }
            },
            link: {
                load: function (item) {

                    if (item.linkCnt == undefined) {
                        return;
                    }

                    $('#modal2_title').html(item.title);
                    $("#modal2").modal('show');

                    var isLoad = false;
                    $("#modal2").on('shown.bs.modal', function (e) {
                        if (isLoad == false) {
                            Apps.data.link.grid.search(item.articleSeq);
                            isLoad = true;
                        }
                    });

                    $("#modal2").on('hidden.bs.modal', function (e) {
                        Apps.grid.target.focus(item.__index);
                    });

                },
                grid: {
                    target: new ax5.ui.grid(),
                    init: function () {

                        Apps.data.link.grid.target.setConfig({
                            target: $('[data-ax5grid="link-grid"]'),
                            columns: [
                                {key: "hrefNm", label: "링크 정보", sortable: true, align: "left", width: 450},
                                {key: "hrefUrl", label: "링크 경로", sortable: true, align: "left", width: 250},
                                {
                                    key: "fileSize", label: "새창 열기", sortable: true, width: 80, formatter: function () {
                                        return "<a href='" + this.item.hrefUrl + "' target='_blank'><i class='fa fa-external-link fa-lg'></i></a>";
                                    }
                                }
                            ],
                            header: {
                                align: "center",
                                columnHeight: 50
                            },
                            body: {
                                align: "center",
                                columnHeight: 28,
                                onClick: function () {
                                }
                            },
                            page: {
                                display: false
                            }
                        });
                    },
                    search: function (articleSeq) {

                        var params = $.param({seq: articleSeq})
                        var url = "${ globalCtx }/collector/rest/retriveCollectorDtaLinkList";

                        /**/
                        $.ajax({
                            url: url,
                            data: params,
                            type: "json",
                            method: "post"
                        }).done(function (res) {

                            if (cdc.code.ok == res.result) {
                                Apps.data.link.grid.target.setData(res.list);
                            } else {
                                cdc.toast.push(res.message, {theme: "danger"});
                            }

                        });
                        /**/

                    }
                }
            }

        }

    };
</script>
<!-- / content-script -->
</body>
</html>
