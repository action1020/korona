<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>수집로그</title>
    <meta name="decorator" content="admin"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-history"></i> 수집로그</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form name="searchForm" id="searchForm" onsubmit="Apps.grid.search(); return false;">
                                    <!-- HIDDEN FIELD[S] -->
                                    <input type="hidden" name="pageNo" value="0"/>
                                    <input type="hidden" name="pageSize" value="500"/>
                                    <!-- HIDDEN FIELD[E] -->

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">수집기 명칭</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control " name="searchWord"
                                                   placeholder="수집기 명칭">
                                        </div>

                                        <label class="col-sm-1 control-label">기간검색</label>
                                        <div class="col-sm-2">

                                            <div class="input-group" data-ax5picker="search1">
                                                <input type="text" name="searchStart" id="searchStart"
                                                       class="form-control" placeholder="yyyy-mm-dd"
                                                       readonly="readonly">
                                                <span class="input-group-addon">~</span>
                                                <input type="text" name="searchEnd" id="searchEnd" class="form-control"
                                                       placeholder="yyyy-mm-dd" readonly="readonly">
                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                            </div>

                                        </div>

                                        <a href="#search" class="btn btn-sm btn-success" role="button"
                                           onclick="Apps.grid.search();"><i class="fa fa-search" aria-hidden="true"></i>
                                            검색</a>

                                    </div>

                                </form>

                            </div>
                        </div>

                        <div data-ax5grid="first-grid" data-ax5grid-config="{}" style="height: 500px;"></div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();
            Apps.grid.init();
            Apps.picker.init();

        },
        init: function () {

            // event binding for searchForm elements
            $('#searchForm > *').bind('keypress', function (e) {

                if ("13" == e.keyCode) {
                    if (e) e.preventDefault();
                    Apps.grid.search();
                }
            });

        },
        grid: {
            target: new ax5.ui.grid(),
            init: function () {

                Apps.grid.target.setConfig({
                    target: $('[data-ax5grid="first-grid"]'),
                    columns: [
                        {key: "rasc", label: "순번"},
                        {key: "clctName", label: "수집기 명칭", sortable: true, align: "left", width: 350},
                        {key: "startDtm", label: "시작시간", sortable: true, width: 200},
                        {key: "finishDtm", label: "종료시간", sortable: true, width: 200},
                        {key: "cnt", label: "수집건수", sortable: true, width: 100}
                    ],
                    header: {
                        align: "center",
                        columnHeight: 50
                    },
                    body: {
                        align: "center",
                        columnHeight: 28,
                        onClick: function () {

                            if (this.colIndex == 10) {
                                Apps.data.execute(this.item);
                            }

                            if (this.colIndex == 11) {
                                Apps.data.bind(this.item);
                            }

                        },
                        onDBLClick: function () {

                            // active via toggle
                            if (this.colIndex == 9) {
                                this.item.active = this.item.active ? false : true;
                                Apps.data.toggle(this.item);
                            }

                        }
                    },
                    page: {
                        navigationItemCount: 9,
                        height: 30,
                        display: true,
                        firstIcon: '<i class="fa fa-step-backward" aria-hidden="true"></i>',
                        prevIcon: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
                        nextIcon: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                        lastIcon: '<i class="fa fa-step-forward" aria-hidden="true"></i>',
                        onChange: function () {
                            Apps.grid.search(this.page.selectPage + 1);
                        }
                    }
                });

                Apps.grid.search();

            },
            search: function (pageNo) {

                $("#searchForm>input[name=pageNo]").val((pageNo - 1) || 0);

                var params = $("#searchForm").serialize();
                var url = "${ globalCtx }/collector/rest/retriveCollectorHisList";

                /**/
                $.ajax({
                    url: url,
                    data: params,
                    type: "json",
                    method: "post"
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {
                        Apps.grid.target.setData(res);
                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });
                /**/

            }
        },
        picker: {
            target: new ax5.ui.picker(),
            init: function () {

                Apps.picker.target.bind({
                    target: $('[data-ax5picker="search1"]'),
                    direction: "top",
                    content: {
                        width: 270,
                        margin: 10,
                        type: 'date',
                        config: {
                            control: {
                                left: '<i class="fa fa-chevron-left"></i>',
                                yearTmpl: '%s',
                                monthTmpl: '%s',
                                right: '<i class="fa fa-chevron-right"></i>'
                            },
                            lang: {
                                yearTmpl: "%s",
                                months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                                dayTmpl: "%s"
                            }
                        }
                    }
                });

                $('#searchStart').val(cdc.date.getWeekStartEndDate()[0]);
                $('#searchEnd').val(cdc.date.getWeekStartEndDate()[1]);
            }
        }

    };
</script>
<!-- / content-script -->
</body>
</html>
