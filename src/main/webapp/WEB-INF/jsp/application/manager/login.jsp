<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Web Console Ver (2020) | Log in</title>
    <sec:csrfMetaTags/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet"
          href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="${ globalCtx }/public/webjars/AdminLTE/2.4.2/plugins/iCheck/square/blue.css">
    <!-- ax5 toast -->
    <link rel="stylesheet" type="text/css" href="${ globalCtx }/public/e2on/ax5ui/ax5toast.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h2 style="margin-bottom: 20px;color:mediumvioletred">Web Console Ver (2020)</h2>
        <form name="login-form" id="login-form" method="post" action="" onsubmit="return Apps.login();">
            <sec:csrfInput/>
            <div class="form-group has-feedback">
                <input type="text" name="loginId" id="loginId" class="form-control" placeholder="아이디">
                <span class="glyphicon glyphicon-console form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="비밀번호">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" id="saveId"> 아이디 저장
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">로그인</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${ globalCtx }/public/webjars/AdminLTE/2.4.2/plugins/iCheck/icheck.min.js"></script>
<!-- AX5 -->
<script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5core.min.js"></script>
<script type="text/javascript" src="${ globalCtx }/public/e2on/ax5ui/ax5toast.min.js"></script>
<!-- Web Console Apps -->
<script src="${ globalCtx }/public/js/apps.js"></script>

<script type="text/javascript">

    var Apps = {
        start: function () {
            Apps.init();
        },
        init: function () {

            $('#saveId').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });

            if (ax5.util.getCookie("userId") != "") {

                $("#saveId").iCheck('check')
                $("#userId").val(ax5.util.getCookie("userId"));
                $("#password").focus();

            } else {
                $("#saveId").iCheck('uncheck')
                $("#userId").focus();
            }

        },
        login: function () {

            var url = "<c:out value='${ globalCtx }/manager/rest/loginCheck'/>";
            var params = $("#login-form").serialize();

            $.ajax({
                url: url,
                data: params,
                type: 'json',
                //contentType : "application/json; charset=UTF-8",
                method: 'post'
            }).done(function (res) {

                if (cdc.code.ok == res.result) {

                    if ($("#saveId").is(":checked")) {
                        ax5.util.setCookie("loginId", $("#userId").val());
                    } else {
                        ax5.util.setCookie("loginId", "");
                    }

                    $("#login-form").prop("action", "<c:out value='${globalCtx}'/>/collector/collectorList");
                    $("#login-form").removeAttr("onsubmit");
                    $("#login-form").submit();

                } else {
                    cdc.toast.push(res.message, {theme: "danger"});
                }

            });

            return false;
        }
    }

    $(document).ready(Apps.start);

</script>
</body>
</html>