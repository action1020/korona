<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>사용자 관리</title>
    <meta name="decorator" content="admin"/>
</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-user-plus"></i> 사용자 관리</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form name="searchForm" id="searchForm" onsubmit="Apps.grid.search(); return false;">
                                    <!-- HIDDEN FIELD[S] -->
                                    <input type="hidden" name="pageNo" value="0"/>
                                    <input type="hidden" name="pageSize" value="500"/>
                                    <!-- HIDDEN FIELD[E] -->

                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">검색조건</label>
                                        <div class="col-sm-2">
                                            <select class="form-control" name="searchOption">
                                                <option value="">-- 선택 --</option>
                                                <option value="1">이름</option>
                                                <option value="2">아이디</option>
                                            </select>
                                        </div>

                                        <label class="col-sm-1 control-label">검색어</label>
                                        <div class="col-sm-2">

                                            <input type="text" class="form-control " name="searchWord"
                                                   placeholder="검색어">

                                        </div>

                                        <a href="#search" class="btn btn-sm btn-success" role="button"
                                           onclick="Apps.grid.search();"><i class="fa fa-search" aria-hidden="true"></i>
                                            검색</a>

                                        <div class="form-group pull-right">
                                            <a href="#add" class="btn btn-sm btn-primary" role="button"
                                               onclick="Apps.modal.open();"><i class="fa fa-plus-square"
                                                                               aria-hidden="true"></i> 추가</a>
                                            <a href="#del" class="btn btn-sm btn-danger" role="button"
                                               onclick="Apps.data.rm();"><i class="fa  fa-minus-square"
                                                                            aria-hidden="true"></i> 삭제</a>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>

                        <div data-ax5grid="first-grid" data-ax5grid-config="{}" style="height: 500px;"></div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" data-backdrop="static"
         data-keyboard="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><strong>사용자 관리</strong></h4>
                </div>

                <div class="modal-body">
                    <form class="form-horizontal" data-toggle="validator" role="form" name="saveForm" id="saveForm">
                        <!-- HIDDEN FIELD[S] -->
                        <input type="hidden" name="id"/>
                        <!-- HIDDEN FIELD[E] -->
                        <div class="form-group">
                            <label class="control-label col-sm-2">사용자 이름</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <input type="text" class="form-control" data-ax-path="name" required="required"
                                           data-error="사용자 이름을 입력하십시오.">
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">사용자 ID</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <input type="text" class="form-control" data-ax-path="loginId" required="required"
                                           data-error="사용자 ID를 입력하십시오.">
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">비밀번호</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <input type="password" class="form-control" data-ax-path="password" name="password"
                                           id="password">
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">비밀번호 확인</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <input type="password" class="form-control" data-ax-path="password2"
                                           name="password2" id="password2" data-match="#password"
                                           data-match-error="비밀번호가 일치하지 않습니다." placeholder="비밀번호 확인">
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">사용자 구분</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <select class="form-control" data-ax-path="grade" required="required"
                                            data-error="Required">
                                        <option value="20">일반</option>
                                        <option value="10">관리자</option>
                                    </select>
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">사용여부</label>
                            <div class="col-sm-10">
                                <div class="required-field-block">
                                    <select class="form-control" data-ax-path="active" required="required"
                                            data-error="Required">
                                        <option value="true">사용</option>
                                        <option value="false">사용안함</option>
                                    </select>
                                    <div class="required-icon">
                                        <div class="text">*</div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer form-group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i> 닫기
                            </button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> 저장</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();
            Apps.grid.init();

        },
        init: function () {

            // event binding for saveForm submit
            $('#saveForm').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    Apps.data.save();
                }
            });

        },
        grid: {
            target: new ax5.ui.grid(),
            init: function () {

                Apps.grid.target.setConfig({
                    target: $('[data-ax5grid="first-grid"]'),
                    showRowSelector: true,
                    multipleSelect: true,
                    columns: [
                        {key: "rasc", label: "번호"},
                        {key: "name", label: "이름", sortable: true, align: "left", width: 150},
                        {key: "loginId", label: "아이디", sortable: true, align: "left", width: 250},
                        {
                            key: "active", label: "사용여부", sortable: true, styleClass: function () {
                                return (this.item.active === false) ? "grid-cell-red" : "grid-cell-blue";
                            }, formatter: function () {
                                return "<a href='#toggle'><i class='fa fa-toggle-" + (this.item.active ? "on" : "off") + "'></i> " + (this.item.active ? "On" : "Off") + "</a>";
                            }
                        },
                        {
                            key: "", label: "수정", sortable: true, width: 50, formatter: function () {
                                return "<a href='#edit'><i class='fa fa-edit fa-lg'></i></a>";
                            }
                        }
                    ],
                    header: {
                        align: "center",
                        columnHeight: 50
                    },
                    body: {
                        align: "center",
                        columnHeight: 28,
                        onClick: function () {

                            if (this.colIndex == 4) {
                                Apps.data.bind(this.item);
                            }

                        },
                        onDBLClick: function () {

                            // bookmark active via toggle
                            if (this.colIndex == 3) {
                                this.item.active = this.item.active ? false : true;
                                Apps.data.toggle(this.item);
                            }

                        }
                    },
                    page: {
                        navigationItemCount: 9,
                        height: 30,
                        display: true,
                        firstIcon: '<i class="fa fa-step-backward" aria-hidden="true"></i>',
                        prevIcon: '<i class="fa fa-caret-left" aria-hidden="true"></i>',
                        nextIcon: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
                        lastIcon: '<i class="fa fa-step-forward" aria-hidden="true"></i>',
                        onChange: function () {
                            Apps.grid.search(this.page.selectPage + 1);
                        }
                    }
                });

                Apps.grid.search();

            },
            search: function (pageNo) {

                $("#searchForm>input[name=pageNo]").val((pageNo - 1) || 0);

                var params = $("#searchForm").serialize();
                var url = "${ globalCtx }/manager/rest/retrieveList";

                $.ajax({
                    url: url,
                    data: params,
                    type: "json",
                    method: "post"
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {
                        Apps.grid.target.setData(res);
                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });

            }
        },
        modal: {
            open: function () {
                Apps.data.target.setModel({}, $("#saveForm"));
                if (Apps.data.target.get("seq") === undefined) {
                    $("#password, #password2").prop("required", true);
                    $('[data-ax-path="userId"]').prop("readonly", false)
                }
                $("#modal1").modal('show');
            },
            close: function () {
                $("#modal1").modal('hide');
            }
        },
        data: {
            target: new ax5.ui.binder(),
            bind: function (obj) {
                Apps.data.target.setModel(obj, $("#saveForm"));
                $("#password, #password2").prop("required", false);
                $('[data-ax-path="userId"]').prop("readonly", true)

                $("#modal1").modal('show');
                $('#saveForm').validator('validate');
            },
            save: function () {

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 저장하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 저장'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            var url = "${ globalCtx }/manager/rest/createOrUpdate";
                            var params = $.param(Apps.data.target.get());

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {

                                    cdc.toast.push("완료되었습니다.", {theme: "info"});
                                    Apps.modal.close();
                                    Apps.grid.search();

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });

                        }

                    }

                });

            },
            rm: function () {

                if (Apps.grid.target.getList("selected").length == 0) {
                    cdc.toast.push("선택된 데이터가 없습니다.", {theme: 'danger'});
                    return;
                }

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 삭제하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 삭제',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            var param = [];

                            $.each(Apps.grid.target.getList("selected"), function () {
                                param.push(this.id);
                            });

                            var url = "${ globalCtx }/manager/rest/removeRows";
                            var params = $.param({checked: param}, true);

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {

                                    cdc.toast.push("완료되었습니다.");
                                    Apps.modal.close();
                                    Apps.grid.search();

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });

                        }

                    }
                });

            },
            toggle: function (item) {

                var url = "${ globalCtx }/manager/rest/createOrUpdate";
                var params = $.param(item);

                $.ajax({
                    url: url,
                    data: params,
                    type: 'json',
                    method: 'post'
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {

                        cdc.toast.push("완료되었습니다.");
                        Apps.grid.search();

                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });

            }
        }
    };
</script>
<!-- / content-script -->
</body>
</html>
