<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>수집통계</title>
    <meta name="decorator" content="admin"/>

    <!-- Load c3.css -->
    <link href="${ globalCtx }/public/e2on/chart/c3.min.css" rel="stylesheet">

    <!-- Load d3.js and c3.js -->
    <script src="${ globalCtx }/public/e2on/chart/d3.v3.min.js" charset="utf-8"></script>
    <script src="${ globalCtx }/public/e2on/chart/c3.min.js"></script>

</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content connectedSortable">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">채널별 전체 수집량</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="panel panel-default" style="height:65px">
                            <div class="panel-body">
                                <form name="searchForm" id="searchForm"
                                      onsubmit="Apps.chart.chart1.search(); return false;">
                                    <div class="form-group pull-right">
                                        <div class="pull-right">
                                            <a href="#search" class="btn btn-sm btn-success" role="button"
                                               onclick="Apps.chart.chart1.search();"><i class="fa fa-search"
                                                                                        aria-hidden="true"></i>
                                                Search</a>
                                        </div>
                                        <div class="col-md-3 pull-right">
                                            <div class="input-group" data-ax5picker="search1">
                                                <input type="text" name="searchStart" id="searchStart"
                                                       class="form-control" placeholder="yyyy-mm-dd"
                                                       readonly="readonly">
                                                <span class="input-group-addon">~</span>
                                                <input type="text" name="searchEnd" id="searchEnd" class="form-control"
                                                       placeholder="yyyy-mm-dd" readonly="readonly">
                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="chart" id="bar-chart" style="height: 400px;"></div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">월별 사전 단어 수집량</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="panel panel-default" style="height:65px">
                            <div class="panel-body">
                                <form name="searchForm2" id="searchForm2"
                                      onsubmit="Apps.chart.chart2.search(); return false;">
                                    <div class="form-group pull-right">
                                        <div class="pull-right">
                                            <a href="#search" class="btn btn-sm btn-success" role="button"
                                               onclick="Apps.chart.chart2.search();"><i class="fa fa-search"
                                                                                        aria-hidden="true"></i>
                                                Search</a>
                                        </div>
                                        <div class="col-md-1 pull-right">
                                            <div class="input-group" data-ax5picker="search2">
                                                <input type="text" name="searchStart2" id="searchStart2"
                                                       class="form-control" placeholder="yyyy" readonly="readonly">
                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="chart" id="line-chart" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">채널별 사전 단어 수집량</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="panel panel-default" style="height:65px">
                            <div class="panel-body">
                                <form name="searchForm3" id="searchForm3"
                                      onsubmit="Apps.chart.chart2.search(); return false;">
                                    <div class="form-group pull-right">
                                        <div class="pull-right">
                                            <a href="#search" class="btn btn-sm btn-success" role="button"
                                               onclick="Apps.chart.chart3.search();"><i class="fa fa-search"
                                                                                        aria-hidden="true"></i>
                                                Search</a>
                                        </div>
                                        <div class="col-md-3 pull-right">
                                            <div class="input-group" data-ax5picker="search3">
                                                <input type="text" name="searchStart3" id="searchStart3"
                                                       class="form-control" placeholder="yyyy-mm-dd"
                                                       readonly="readonly">
                                                <span class="input-group-addon">~</span>
                                                <input type="text" name="searchEnd3" id="searchEnd3"
                                                       class="form-control" placeholder="yyyy-mm-dd"
                                                       readonly="readonly">
                                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="box-body" id="donut-chart">
                            DONUT
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();

            Apps.chart.chart1.search();
            Apps.chart.chart2.search();
            Apps.chart.chart3.search();

        },
        init: function () {

            // Make the chart sortable Using jquery UI
            $('.connectedSortable').sortable({
                placeholder: 'sort-highlight',
                connectWith: '.connectedSortable',
                handle: '.box-header, .nav-tabs',
                forcePlaceholderSize: true,
                zIndex: 999999
            });

            Apps.picker.picker1.init();
            Apps.picker.picker2.init();
            Apps.picker.picker3.init();

        },
        picker: {
            picker1: {
                target: new ax5.ui.picker(),
                init: function () {

                    Apps.picker.picker1.target.bind({
                        target: $('[data-ax5picker="search1"]'),
                        direction: "top",
                        content: {
                            width: 270,
                            margin: 10,
                            type: 'date',
                            config: {
                                control: {
                                    left: '<i class="fa fa-chevron-left"></i>',
                                    yearTmpl: '%s',
                                    monthTmpl: '%s',
                                    right: '<i class="fa fa-chevron-right"></i>'
                                },
                                lang: {
                                    yearTmpl: "%s",
                                    months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                                    dayTmpl: "%s"
                                }
                            }
                        }
                    });

                    $('#searchStart').val(cdc.date.getWeekStartEndDate()[0]);
                    $('#searchEnd').val(cdc.date.getWeekStartEndDate()[1]);
                }
            },
            picker2: {
                target: new ax5.ui.picker(),
                init: function () {

                    Apps.picker.picker1.target.bind({
                        target: $('[data-ax5picker="search2"]'),
                        content: {
                            type: 'date',
                            config: {
                                mode: "year", selectMode: "year"
                            },
                            formatter: {
                                pattern: 'date(year)'
                            }
                        }
                    });

                    $('#searchStart2').val(cdc.date.getYear());
                }
            },
            picker3: {
                target: new ax5.ui.picker(),
                init: function () {

                    Apps.picker.picker3.target.bind({
                        target: $('[data-ax5picker="search3"]'),
                        direction: "top",
                        content: {
                            width: 270,
                            margin: 10,
                            type: 'date',
                            config: {
                                control: {
                                    left: '<i class="fa fa-chevron-left"></i>',
                                    yearTmpl: '%s',
                                    monthTmpl: '%s',
                                    right: '<i class="fa fa-chevron-right"></i>'
                                },
                                lang: {
                                    yearTmpl: "%s",
                                    months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                                    dayTmpl: "%s"
                                }
                            }
                        }
                    });

                    $('#searchStart3').val(cdc.date.firstDayOfMonth());
                    $('#searchEnd3').val(cdc.date.lastDayOfMonth());

                }
            }
        },
        chart: {
            chart1: {
                search: function (o) {

                    var params = $("#searchForm").serialize();
                    var url = "${ globalCtx }/statistics/rest/retreiveCollectionByChannel";

                    $.ajax({
                        url: url,
                        data: params,
                        type: "json",
                        method: "post"
                    }).done(function (res) {

                        if (cdc.code.ok == res.result) {

                            var rows = [], cols = [], rs = [], max = 0;

                            // find by last code value
                            $.each(res.list, function (i, item) {
                                if (item.cdVal > max) {
                                    max = item.cdVal;
                                    $.noop();
                                }
                            });

                            // dataset
                            $.each(res.list, function (i, item) {

                                rs['y'] = item.clctDtm;
                                rs[item.cdNm] = item.cnt;

                                if ($.inArray(item.cdNm, cols) === -1) {
                                    cols.push(item.cdNm);
                                }

                                if (item.cdVal == max) {
                                    rows.push(rs);
                                    rs = [];
                                }
                            });


                            var chart = c3.generate({
                                bindto: '#bar-chart',
                                data: {
                                    type: 'bar',
                                    json: rows,
                                    keys: {
                                        x: 'y', // it's possible to specify 'x' when category axis
                                        value: cols
                                    }
                                },
                                axis: {
                                    x: {
                                        type: 'timeseries',
                                        tick: {
                                            format: '%Y-%m-%d'
                                        }
                                    }
                                }
                            });

                        } else {
                            cdc.toast.push(res.message, {theme: "danger"});
                        }

                    });
                }
            },
            chart2: {
                search: function () {
                    var params = $("#searchForm2").serialize();
                    var url = "${ globalCtx }/statistics/rest/retreiveCollectionByDicMonthly";

                    $.ajax({
                        url: url,
                        data: params,
                        type: "json",
                        method: "post"
                    }).done(function (res) {

                        if (cdc.code.ok == res.result) {

                            var rows = [], cols = [], rs = [];

                            $.each(res.list, function (i, item) {

                                rs['y'] = item.clctDtm;

                                if (item.kwdName != '') {
                                    rs[item.kwdName] = item.cnt;
                                    if ($.inArray(item.kwdName, cols) === -1) {
                                        cols.push(item.kwdName);
                                    }
                                }

                                if ((res.list.length - 1) > i) {

                                    if (res.list[i + 1].clctDtmOdr > item.clctDtmOdr) {
                                        rows.push(rs);
                                        rs = [];
                                    }
                                } else if ((res.list.length - 1) == i) {
                                    rows.push(rs);
                                    rs = [];
                                }

                            });

                            var chart = c3.generate({
                                bindto: '#line-chart',
                                data: {
                                    type: 'line',
                                    json: rows,
                                    keys: {
                                        x: 'y', // it's possible to specify 'x' when category axis
                                        value: cols
                                    }
                                },
                                axis: {
                                    x: {
                                        type: 'category'
                                    }
                                }

                            });

                        } else {
                            cdc.toast.push(res.message, {theme: "danger"});
                        }

                    });
                }

            },
            chart3: {

                search: function () {

                    var params = $("#searchForm3").serialize();
                    var url = "${ globalCtx }/statistics/rest/retrieveCollectionByDicByChannel";

                    $.ajax({
                        url: url,
                        data: params,
                        type: "json",
                        method: "post"
                    }).done(function (res) {


                        if (cdc.code.ok == res.result) {

                            $("#donut-chart").empty();
                            var rows = [], cols = [], rs = [];

                            $.each(res.list, function (i, item) {

                                if (item.kwdName != '') {
                                    rs['cdNm'] = item.cdNm;
                                    rs['cdVal'] = item.cdVal;

                                    rs['cols'] = cols;

                                    var cd = [];
                                    cd.push(item.kwdName + " (" + item.cnt + ")건");
                                    cd.push(item.cnt)

                                    cols.push(cd);

                                }

                                if ((res.list.length - 1) > i) {

                                    if (res.list[i + 1].cdVal > item.cdVal) {
                                        rows.push(rs);
                                        rs = [];
                                        cols = [];
                                    }
                                } else if ((res.list.length - 1) == i) {
                                    rows.push(rs);
                                    rs = [];
                                    cols = [];

                                }

                            });

                            $.each(rows, function (i) {
                                Apps.chart.chart3.draw(this, i);
                            })


                        } else {
                            cdc.toast.push(res.message, {theme: "danger"});
                        }

                    });
                },

                draw: function (obj, i) {

                    $("#donut-chart").append("<div class=\"chart\" id=\"donut-chart" + i + "\" style=\"height: 300px;\"></div>");

                    var chart = c3.generate({
                        bindto: '#donut-chart' + i,
                        donut: {
                            title: obj.cdNm

                        },
                        data: {
                            type: 'donut',
                            columns: obj.cols
                        },
                        legend: {
                            position: 'right'
                        }
                    });

                }

            }
        }
    };


</script>
<!-- / content-script -->

</body>
</html>
