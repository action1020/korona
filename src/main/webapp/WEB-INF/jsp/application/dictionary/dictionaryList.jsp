<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/includes/taglibs.jsp" %>
<html>
<head>
    <title>사전관리</title>
    <meta name="decorator" content="admin"/>

    <!-- jstree -->
    <link href="${ globalCtx }/public/e2on/jstree/themes/default/style.min.css" rel="stylesheet">
    <script src="${ globalCtx }/public/e2on/jstree/jstree.min.js" charset="utf-8"></script>
    <!-- toggle -->
    <link href="${ globalCtx }/public/e2on/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="${ globalCtx }/public/e2on/js/bootstrap-toggle.min.js" charset="utf-8"></script>
    <!-- fileupload -->
    <link href="${ globalCtx }/public/e2on/css/jquery.fileupload.css" rel="stylesheet">
    <script src="${ globalCtx }/public/e2on/js/jquery.iframe-transport.js" charset="utf-8"></script>
    <script src="${ globalCtx }/public/e2on/js/jquery.fileupload.js" charset="utf-8"></script>

</head>
<body>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-danger">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-book"></i> 사전관리</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body">

                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-sm-1 control-label">검색</label>
                                    <div class="col-sm-4 ">
                                        <input type="text" class="form-control " name="searchWord" placeholder="검색어">
                                    </div>
                                    <div class="form-group pull-right">
                                        <input type="checkbox" id="expand" data-toggle="toggle" data-on="펼침"
                                               data-off="닫힘" data-size="mini" data-height="30">
                                        <a href="#search" class="btn btn-sm btn-warning" role="button"
                                           onclick="Apps.jstree.rename();"><i class="fa fa-refresh"
                                                                              aria-hidden="true"></i> 이름변경</a>
                                        <a href="#search" class="btn btn-sm btn-bitbucket" role="button"
                                           onclick="Apps.jstree.add(true);"><i class="fa fa-plus-square"
                                                                               aria-hidden="true"></i> 최상위</a>
                                        <a href="#add" class="btn btn-sm btn-primary" role="button"
                                           onclick="Apps.jstree.add(false);"><i class="fa fa-plus-square"
                                                                                aria-hidden="true"></i> 자식</a>
                                        <a href="#del" class="btn btn-sm btn-danger" role="button"
                                           onclick="Apps.jstree.rm();"><i class="fa fa-minus-square"
                                                                          aria-hidden="true"></i> 삭제</a>
                                        <a href="#save" class="btn btn-sm btn-primary" role="button"
                                           onclick="Apps.jstree.save();"><i class="fa fa-check" aria-hidden="true"></i>
                                            저장</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="jstree" style="overflow-y: auto; height: 424px;"></div>

                        <div class="modal-footer form-group">
                            <span class="btn btn-sm btn-danger fileinput-button"><i
                                    class="glyphicon glyphicon-plus"></i> 엑셀 업로드<input id="fileupload" type="file"
                                                                                       name="file"></span>
                            <a href="${ globalCtx }/dictionary/excel" target="_blank" class="btn btn-sm btn-primary"
                               role="button"><i class="fa fa-check" aria-hidden="true"></i> 엑셀 다운로드</a>
                        </div>

                        <!-- The global progress bar -->
                        <div id="progress" class="progress" style="display:none">
                            <div class="progress-bar progress-bar-success"></div>
                        </div>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <!-- box-header -->
                    <div class="box-header">
                        <h3 class="box-title" id="title">&nbsp;</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- box-body -->
                    <div class="box-body" id="detail" style="height: 617px">

                        <form class="form-horizontal" data-toggle="validator" role="form" name="saveForm" id="saveForm">
                            <!-- HIDDEN FIELD[S] -->
                            <input type="hidden" name="id" data-ax-path="id"/>
                            <!-- HIDDEN FIELD[E] -->
                            <div class="form-group">
                                <label for="text" class="control-label col-sm-2">이름</label>
                                <div class="col-sm-10">
                                    <div class="required-field-block">
                                        <input type="text" class="form-control" data-ax-path="text"
                                               data-error="Node name is mandatory." readonly="readonly" id="text">
                                        <div class="required-icon">
                                            <div class="text">*</div>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="korean" class="control-label col-sm-2">한국어</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" data-ax-path="korean" id="korean">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="english" class="control-label col-sm-2">영어</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" data-ax-path="english" id="english">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="japanese" class="control-label col-sm-2">일본어</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" data-ax-path="japanese" id="japanese">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="chinaSimple" class="control-label col-sm-2">중국어 간체</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" data-ax-path="chinaSimple" id="chinaSimple">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="chinaBun" class="control-label col-sm-2">중국어</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" data-ax-path="chinaBun" id="chinaBun">
                                </div>
                            </div>

                            <div class="modal-footer form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>저장</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<!-- content-script -->
<script type="text/javascript">
    var Apps = {
        start: function () {

            Apps.init();
            Apps.jstree.load();
            Apps.data.excel();

        },
        init: function () {

            // detail word from disabled
            $("#detail").find('input').prop("disabled", true);

            // event binding for saveForm submit
            $('#saveForm').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    Apps.data.save();
                }
            });

            // search jstree
            $("input[name=searchWord]").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    Apps.jstree.target.jstree(true).search($(this).val());
                }
            });

            // expanded
            $('#expand').bootstrapToggle();

            $('#expand').change(function () {
                var $expand = $(this).prop('checked');
                if ($expand) {
                    Apps.jstree.target.jstree('open_all');
                } else {
                    Apps.jstree.target.jstree('close_all');
                }
            })

        },
        jstree: {
            target: $('#jstree'),
            add: function (root) {

                var ref = Apps.jstree.target.jstree(true), sel = ref.get_selected();

                if (!sel.length || root) {
                    // root
                    Apps.jstree.target.jstree(true).deselect_all();
                    var id = Apps.jstree.target.jstree().create_node('#', {
                        "id": "",
                        "text": "ROOT",
                        "data": {mode: "I"},
                        "type": "root"
                    }, "first");
                    Apps.jstree.target.jstree('select_node', '#' + id);
                    sel = ref.get_selected();

                    if (sel.length == 0) {
                        cdc.toast.push("최상위 노드 이름을 변경하신 후 작업하십시오.", {theme: "danger"});
                    }

                } else {

                    sel = sel[0];
                    sel = ref.create_node(sel, {'data': {mode: 'I'}});

                }

                if (sel) {
                    ref.edit(sel);
                }

            },
            rm: function () {

                var ref = Apps.jstree.target.jstree(true), sel = ref.get_selected();

                if (!sel.length) {
                    return false;
                }

                var mode = ref.get_node(sel).data.mode, children = ref.get_node(sel).children;

                if (children.length > 0) {
                    var childrenDeleted = 0;
                    for (var i = 0; i < children.length; i++) {
                        if (ref.get_node(children[i]).data.mode == 'D') {
                            childrenDeleted++;
                        }
                    }

                    if (children.length != childrenDeleted) {
                        cdc.toast.push("해당 노드는 자식노드를 가지고 있습니다.", {theme: 'danger'});
                        return;
                    }
                }

                if (mode == 'I') {
                    ref.delete_node(sel);
                } else {
                    ref.get_node(sel).data = {mode: 'D'};
                    ref.hide_node(sel);
                }

            },
            save: function () {

                var hasUpdateDate = false;
                $.each($(Apps.jstree.target).jstree(true).get_json('#', {flat: true}), function () {
                    if ('' !== this.data.mode) {
                        hasUpdateDate = true;
                        return true;
                    }
                });

                if (!hasUpdateDate) {
                    cdc.toast.push("변경된 데이터가 없습니다.", {theme: 'danger'});
                    return;
                }

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 저장 하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 저장'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            var url = "${ globalCtx }/dictionary/rest/createOrUpdate";
                            var params = JSON.stringify($(Apps.jstree.target).jstree(true).get_json('#', {
                                flat: true,
                                no_state: true,
                                no_li_attr: true,
                                no_a_attr: true
                            }));

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                contentType: "application/json; charset=UTF-8",
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {
                                    // detail word from disabled
                                    $("#title").html("");
                                    Apps.data.bind({});
                                    $("#detail").find('input').prop("disabled", true);

                                    // reload
                                    Apps.jstree.load();
                                    cdc.toast.push("완료되었습니다.");

                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });
                        }

                    }
                });

            },
            opts: {
                plugins: ["wholerow", "unique", "search", "types", "ui", "dnd"],
                core: {
                    multiple: false,
                    animation: 500,
                    check_callback: function (op, node, par, pos, more) {

                        if (more && more.dnd && (op === 'move_node') && (par.parent == null || node.parent == '#')) {
                            return false;
                        }

                        return true;
                    }
                },
                search: {
                    case_insensitive: true,
                    show_only_matches: true
                },
                types: {
                    root: {
                        icon: "fa fa-database",
                        max_depth: 1
                    },
                    default: {
                        icon: "fa fa-folder"
                    }
                }
            },
            load: function () {

                var url = "${ globalCtx }/dictionary/rest/retrieveTreeList";
                var params = "";

                $.ajax({
                    url: url,
                    data: params,
                    type: 'json',
                    method: 'post'
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {

                        // append mode data to original data structure
                        $.each(res.list, function () {
                            this['data'] = {mode: ''};
                            this['mode'] = '';
                        });

                        Apps.jstree.opts.core.data = []; // clear core data
                        var $opts = $.extend(true, Apps.jstree.opts, {"core": {"data": res.list}});

                        Apps.jstree.target.jstree('destroy').jstree($opts);
                        Apps.jstree.target.bind('rename_node.jstree', function (evt, obj) {

                            if (obj.node.data.mode == '' && obj.old != obj.text) {
                                obj.node.data = {mode: 'U'};
                            }

                        }).bind("move_node.jstree", function (evt, obj) {

                            if (obj.node.data.mode != 'I') {
                                obj.node.data = {mode: 'U'}
                            }

                        }).bind("ready.jstree", function () {

                            var $expand = $('#expand').prop('checked');
                            if ($expand) {
                                Apps.jstree.target.jstree('open_all');
                            } else {
                                Apps.jstree.target.jstree('close_all');
                            }

                        }).bind("select_node.jstree", function (evt, obj) {

                            if (!obj.selected) {
                                $("#title").html('');
                                return;
                            }

                            if (obj.node.text == "ROOT") {
                                return;
                            }

                            if (obj.node.data.mode == 'I' || obj.node.data.mode == 'D') {
                                $("#title").html('저장되지 않은 데이터가 있습니다. 저장 이후 계속 진행하십시오.');
                                cdc.toast.push("저장되지 않은 데이터가 있습니다. 저장 이후 계속 진행하십시오.", {theme: "danger"});
                                Apps.data.bind({});
                                return;
                            }

                            $("#detail").find('input').prop("disabled", false);
                            $("#title").html(obj.node.text);

                            var params = {
                                id: obj.node.id,
                                text: obj.node.text
                            };

                            Apps.data.bind(params);
                            Apps.data.load();

                        });

                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });

            },
            rename: function () {

                var ref = Apps.jstree.target.jstree(true), sel = ref.get_selected();

                if (!sel.length) {
                    cdc.toast.push("선택된 노드가 없습니다.", {theme: 'info'});
                    return;
                }

                if (sel) {
                    ref.edit(sel);
                }
            }
        },
        data: {
            target: new ax5.ui.binder(),
            bind: function (obj) {
                Apps.data.target.setModel(obj, $("#saveForm"));
                $('#saveForm').validator('validate');
            },
            save: function () {

                if (Apps.data.target.get().id === undefined) {
                    cdc.toast.push("저장되지 않은 데이터가 있습니다. 저장 이후 계속 진행하십시오.", {theme: "danger"});
                    return;
                }

                bootbox.confirm({
                    message: "<b><i class='fa fa-arrow-right'></i> 저장 하시겠습니까?</b>",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> 닫기'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> 저장'
                        }
                    },
                    callback: function (result) {

                        if (result) {

                            var url = "${ globalCtx }/dictionary/rest/createOrUpdateSynonym";
                            var params = $.param(Apps.data.target.get());

                            $.ajax({
                                url: url,
                                data: params,
                                type: 'json',
                                method: 'post'
                            }).done(function (res) {

                                if (cdc.code.ok == res.result) {
                                    cdc.toast.push("완료되었습니다.");
                                } else {
                                    cdc.toast.push(res.message, {theme: "danger"});
                                }

                            });

                        }

                    }

                });
            },
            load: function () {

                var url = "${ globalCtx }/dictionary/rest/retrieveSynonym";
                var params = "seq=" + Apps.data.target.get().id;

                $.ajax({
                    url: url,
                    data: params,
                    type: 'json',
                    method: 'post',
                    global: false
                }).done(function (res) {

                    if (cdc.code.ok == res.result) {
                        Apps.data.bind($.extend({}, Apps.data.target.get(), res.data));
                    } else {
                        cdc.toast.push(res.message, {theme: "danger"});
                    }

                });
            },
            excel: function () {

                var url = "${ globalCtx }/dictionary/rest/excelUpload";

                $('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    maxFile: 1,
                    singleFileUploads: true,
                    done: function (e, data) {

                        if (cdc.code.ok == data.result.result) {


                            cdc.toast.push("완료되었습니다.");
                            Apps.jstree.load();

                        } else {
                            cdc.toast.push(data.result.message, {theme: "danger"});
                        }

                        $('#progress .progress-bar').css('width', '0%');
                        $("#progress").hide();
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css('width', progress + '%');
                    },
                    add: function (e, data) {

                        var file = data.files[0].name;

                        if (file.match(/(\.|\/)(xls|xlsx)$/i) == null) {
                            cdc.toast.push("엑셀 확장자만 허용합니다.", {theme: "danger"});
                            return;
                        }

                        $("#progress").show();
                        data.submit();
                    }

                }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

            }
        }
    };
</script>
<!-- / content-script -->
</body>
</html>