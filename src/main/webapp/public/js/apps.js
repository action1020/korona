/**
 * E2ON Web Application
 * ------------------
 */
$(function () {
    'use strict';

    /**
     * List of all the available skins
     *
     * @type Array
     */
    var mySkins = [
        'skin-blue', 'skin-black', 'skin-red', 'skin-yellow', 'skin-purple', 'skin-green', 'skin-blue-light', 'skin-black-light', 'skin-red-light', 'skin-yellow-light', 'skin-purple-light', 'skin-green-light'
    ];

    /**
     * Get a prestored setting
     *
     * @param String name Name of of the setting
     * @returns String The value of the setting | null
     */
    function get(name) {
        if (typeof (Storage) !== 'undefined') {
            return localStorage.getItem(name);
        } else {
            window.alert('Please use a modern browser to properly view this template!');
        }
    }

    /**
     * Store a new settings in the browser
     *
     * @param String name Name of the setting
     * @param String val Value of the setting
     * @returns void
     */
    function store(name, val) {
        if (typeof (Storage) !== 'undefined') {
            localStorage.setItem(name, val);
        } else {
            window.alert('Please use a modern browser to properly view this template!');
        }
    }

    /**
     * Replaces the old skin with the new skin
     * @param String cls the new skin class
     * @returns Boolean false to prevent link's default action
     */
    function changeSkin(cls) {
        $.each(mySkins, function (i) {
            $('body').removeClass(mySkins[i]);
        });

        $('body').addClass(cls);
        store('skin', cls);
        return false;
    }

    /**
     * Retrieve default settings and apply them to the template
     *
     * @returns void
     */
    function setup() {

        if (get('skin')) {
            changeSkin(get('skin'));
        } else {
            changeSkin('skin-black-light');
        }

        //  Reset options
        if ($('body').hasClass('fixed')) {
            $('[data-layout="fixed"]').attr('checked', 'checked');
        }
        if ($('body').hasClass('layout-boxed')) {
            $('[data-layout="layout-boxed"]').attr('checked', 'checked');
        }
        if ($('body').hasClass('sidebar-collapse')) {
            $('[data-layout="sidebar-collapse"]').attr('checked', 'checked');
        }

    }

    // set up required filed
    $('div>.required-icon').tooltip({
        placement: 'left',
        title: 'Required field'
    });

    // set up application start
    try {
        $(document).ready(Apps.start);
    } catch (e) {
    }

    // bookmark start
    try {
        $(document).ready(Bookmark.start);
    } catch (e) {
    }

});

var cdc = {
    code: {
        ok: "ok",
        no: "no"
    },
    toast: {
        target: undefined,
        push: function (msg, config) {

            var _config = {
                icon: '<i class="fa fa-bell"></i>',
                containerPosition: "bottom-right",
                closeIcon: '<i class="fa fa-times"></i>',
                theme: 'info' // "primary", "success", "info", "warning", "danger"
            };

            if (cdc.toast.target === undefined) {
                cdc.toast.target = new ax5.ui.toast();
            }

            if (config === undefined) {
                cdc.toast.target.setConfig(_config);
            } else {
                $.extend(_config, config);
            }

            cdc.toast.target.setConfig(_config);
            cdc.toast.target.push(msg);
        }
    },
    message: {
        saveConfirm: "<b><i class='fa fa-arrow-right'></i> Do you want to save it?</b>",
        saveSuccess: "Completed",
        notselected: "No data selected."
    },
    date: {
        date: new Date(),
        firstDayOfMonth: function () {
            // 현재 월의 첫째날(01)을 리턴한다. 2012-07-01
            var date = cdc.date.date;
            date.setDate(1);
            return cdc.date.getDate(date);
        },
        lastDayOfMonth: function () {
            // 현재 월의 마지막날(31)을 리턴한다. 2012-07-31
            var date = cdc.date.date;
            var month = parseInt(date.getMonth()) + 1;
            var last = new Date(date.getFullYear(), (month < 10 ? "0" + month : month), 0).getDate();
            date.setDate(last);
            return cdc.date.getDate(date);
        },
        firstDayOfWeek: function () {
            return cdc.date.getWeekStartEndDate()[0];
        },
        lastDayOfWeek: function () {
            return cdc.date.getWeekStartEndDate()[1];
        },
        getWeek: function () {
            // 현재일자의 주차를 리턴한다.
            var date = new Date(new Date().getFullYear(), 0, 1);
            return Math.ceil((((new Date() - date) / 86400000) + date.getDay() + 1) / 7);
        },
        getYear: function () {
            // 현재 년도를 리턴 2012
            return this.date.getFullYear();
        },
        getMonth: function (date) {
            date = date || cdc.date.date;
            return parseInt(date.getMonth()) + 1;
        },
        getDay: function (date) {
            date = date || cdc.date.date;
            return date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
        },
        getWeekStartEndDate: function () {
            // 현재 주차의 시작일자(월) 부터 종료일자(금) 까지의 날자를 리턴한다.
            var start = 0;
            //var today = new Date(this.setHours(0, 0, 0, 0));
            var today = new Date();

            var day = today.getDay() - start;
            var date = today.getDate() - day;

            var StartDate = new Date(today.setDate(date + 1));
            var EndDate = new Date(today.setDate(date + 5));

            return [cdc.date.getDate(StartDate), cdc.date.getDate(EndDate)];
        },
        getDate: function (date) {
            date = date || cdc.date.date;
            var month = parseInt(date.getMonth()) + 1;
            var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
            return date.getFullYear() + "-" + (month < 10 ? "0" + month : month) + "-" + day;
        },
        getYesterday: function (date) {
            date = date || cdc.date.getDate();

            var selectDate = date.split("-");
            var changeDate = new Date();
            changeDate.setFullYear(selectDate[0], selectDate[1] - 1, selectDate[2] - 1);

            var y = changeDate.getFullYear();
            var m = changeDate.getMonth() + 1;
            var d = changeDate.getDate();
            if (m < 10) {
                m = "0" + m;
            }
            if (d < 10) {
                d = "0" + d;
            }

            return y + "-" + m + "-" + d;
        },
        getPrevYearMonth: function (date) {
            var date = date || new Date();

            date.setMonth(date.getMonth() - 1);

            var y = date.getFullYear();
            var m = date.getMonth() + 1;

            if (m < 10) {
                m = "0" + m;
            }

            return y + "-" + m;

        },
        getPrevWeekDate: function (date) {
            var date = date || new Date();

            date.setDate(date.getDate() - 7);
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();

            if (m < 10) {
                m = "0" + m;
            }
            if (d < 10) {
                d = "0" + d;
            }

            return y + "-" + m + "-" + d;
        },
        getDt: function (ds, hs) {
            if (!ds) ds = "-";
            if (!hs) hs = ":";

            var date = new Date();
            var ymd = cdc.date.getDate(date);
            var hms = (date.getHours() < 10 ? "0" + date.getHours() : date.getHours());
            hms += hs + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
            hms += hs + (date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds());

            return ds == "-" ? ymd + " " + hms : ymd.split('-').join(ds) + " " + hms;
        },
        getHour: function () {

            var date = new Date();
            var ymd = cdc.date.getDate(date);
            var hour = (date.getHours() < 10 ? "0" + date.getHours() : date.getHours());

            return hour;
        },
        getMinute: function () {

            var date = new Date();
            var ymd = cdc.date.getDate(date);
            var min = (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());

            return min;
        },
        getSecond: function () {

            var date = new Date();
            var ymd = cdc.date.getDate(date);
            var sec = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

            return sec;
        }
    },
    data: {
        getFileSize: function (bytes, decimals) {

            if (bytes == 0 || bytes === undefined) return '0 Bytes';

            var k = 1024, dm = decimals || 2;
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            var i = Math.floor(Math.log(bytes) / Math.log(k));

            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

        },
        passwordChange: function (ctx) {

            var url = ctx + "/manager/rest/passwordUpdate";
            var params = $("#passForm").serialize();

            if ($.trim(document.passForm.password.value) == '') {
                cdc.toast.push("신규 비밀번호를 입력하세요.", {theme: "warning"});
                return;
            }

            if ($.trim(document.passForm.password2.value) == '') {
                cdc.toast.push("신규 비밀번호를 입력하세요.", {theme: "warning"});
                return;
            }

            if (document.passForm.password.value != document.passForm.password2.value) {
                cdc.toast.push("비밀번호가 서로 다릅니다.", {theme: "warning"});
                return;
            }

            $.ajax({
                url: url,
                data: params,
                type: "json",
                method: "post"
            }).done(function (res) {

                if (cdc.code.ok == res.result) {
                    document.passForm.password.value = "";
                    document.passForm.password2.value = "";
                    cdc.toast.push("비밀번호를 변경하였습니다.", {theme: "primary"});
                } else {
                    cdc.toast.push(res.message, {theme: "danger"});
                }

            });

        }
    }
};

/*
 *  Cross Site Request Forgery (CSRF)
 *  protected CSRF for ajax Jquery
 */

(function ($) {

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $.ajaxSetup({
        beforeSend: function (xhr) {
            $("#GlobalLoading").modal('show');
            if (token && header) {
                xhr.setRequestHeader(header, token);
                xhr.setRequestHeader("E2ON_AJAX", true);
            }
        },
        complete: function () {
            $("#GlobalLoading").modal('hide');
        },
        error: function () {
            cdc.toast.push("An error occurred during server communication.\nAll Please try again later.", {theme: "danger"});
        },
        statusCode: {
            403: function () {
                alert("로그아웃 되었습니다.");
                location.reload();

            }
        }
    });

})(jQuery);