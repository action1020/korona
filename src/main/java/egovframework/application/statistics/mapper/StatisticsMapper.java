package egovframework.application.statistics.mapper;

import egovframework.application.domain.SearchVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;

/**
 * Created by action1020s@gmail.com on 2018-03-29
 */
@Mapper(value = "statisticsMapper")
public interface StatisticsMapper {

    /**
     * retreiveCollectionByChannel
     *
     * @param searchVO
     * @return
     */
    List<EgovMap> retreiveCollectionByChannel(SearchVO searchVO) throws Exception;

    /**
     * retreiveCollectionByDicMonthly
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retreiveCollectionByDicMonthly(SearchVO searchVO) throws Exception;

    /**
     * retrieveCollectionByDicByChannel
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retrieveCollectionByDicByChannel(SearchVO searchVO) throws Exception;
}