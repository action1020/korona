package egovframework.application.statistics.service;

import egovframework.application.domain.SearchVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 9.
 * @description StatisticsService.java
 */
public interface StatisticsService {

    /**
     * retreiveCollectionByChannel
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retreiveCollectionByChannel(SearchVO searchVO) throws Exception;

    /**
     * retreiveCollectionByDicMonthly
     *
     * @param searchVO
     * @return
     */
    List<EgovMap> retreiveCollectionByDicMonthly(SearchVO searchVO) throws Exception;

    /**
     * retrieveCollectionByDicByChannel
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retrieveCollectionByDicByChannel(SearchVO searchVO) throws Exception;
}
