package egovframework.application.statistics.service.impl;

import egovframework.application.domain.SearchVO;
import egovframework.application.statistics.mapper.StatisticsMapper;
import egovframework.application.statistics.service.StatisticsService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 9.
 * @description StatisticsServiceImpl.java
 */
@Slf4j
@Service("statisticsService")
public class StatisticsServiceImpl extends EgovAbstractServiceImpl implements StatisticsService {

    @Resource(name = "statisticsMapper")
    private StatisticsMapper statisticsMapper;

    @Override
    public List<EgovMap> retreiveCollectionByChannel(SearchVO searchVO) throws Exception {
        log.debug("retreiveCollectionByChannel");
        return statisticsMapper.retreiveCollectionByChannel(searchVO);
    }

    /**
     * retreiveCollectionByDicMonthly
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    @Override
    public List<EgovMap> retreiveCollectionByDicMonthly(SearchVO searchVO) throws Exception {
        log.debug("retreiveCollectionByDicMonthly");
        return statisticsMapper.retreiveCollectionByDicMonthly(searchVO);
    }

    /**
     * retrieveCollectionByDicByChannel
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    @Override
    public List<EgovMap> retrieveCollectionByDicByChannel(SearchVO searchVO) throws Exception {
        log.debug("retrieveCollectionByDicByChannel");
        return statisticsMapper.retrieveCollectionByDicByChannel(searchVO);
    }
}