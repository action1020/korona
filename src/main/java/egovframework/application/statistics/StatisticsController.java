package egovframework.application.statistics;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 8.
 * @description StatisticsController.java
 */
@Slf4j
@Controller
@RequestMapping(value = "/statistics/")
public class StatisticsController {

    /**
     * retrieveList
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "retrieveList")
    public String retrieveList(ModelMap model) {
        log.debug("list");
        return "/statistics/statisticsList";
    }

}