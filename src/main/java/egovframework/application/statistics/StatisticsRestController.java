package egovframework.application.statistics;

import egovframework.application.domain.SearchVO;
import egovframework.application.statistics.service.StatisticsService;
import egovframework.framework.contants.Ajax;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 9.
 * @description StatisticsRestController.java
 */
@Slf4j
@RestController
@RequestMapping(value = "/statistics/rest/", produces = "application/json", method = RequestMethod.POST)
public class StatisticsRestController {

    @Resource(name = "statisticsService")
    private StatisticsService statisticsService;

    /**
     * retreiveCollectionByChannel
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "retreiveCollectionByChannel")
    public EgovMap retreiveCollectionByChannel(SearchVO searchVO) {

        log.debug("retreiveCollectionByChannel");
        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = statisticsService.retreiveCollectionByChannel(searchVO);

            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;

    }

    /**
     * retreiveCollectionByDicMonthly
     *
     * @param searchVO
     * @return
     */
    @RequestMapping(value = "retreiveCollectionByDicMonthly")
    public EgovMap retreiveCollectionByDicMonthly(SearchVO searchVO) {

        log.debug("retreiveCollectionByDicMonthly");
        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = statisticsService.retreiveCollectionByDicMonthly(searchVO);

            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    /**
     * retrieveCollectionByDicByChannel
     *
     * @param searchVO
     * @return
     */
    @RequestMapping(value = "retrieveCollectionByDicByChannel")
    public EgovMap retrieveCollectionByDicByChannel(SearchVO searchVO) {

        log.debug("retrieveCollectionByDicByChannel");
        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = statisticsService.retrieveCollectionByDicByChannel(searchVO);

            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;

    }
}
