package egovframework.application.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 3. 14.
 * @description CollectorLinkVO.java
 */
@Getter
@Setter
@ToString
public class CollectorLinkVO extends CommonVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long articleSeq;
    private String hrefNm;
    private String hrefUrl;

}