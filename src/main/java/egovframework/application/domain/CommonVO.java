package egovframework.application.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description CommonVO.java
 */
@Getter
@Setter
@ToString
public class CommonVO extends AbstractCommonVO {

    private Long[] checked;
    private Long seq;
    private Integer rasc;
    private Integer rdesc;
    private Boolean excel;

}