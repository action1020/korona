package egovframework.application.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description CollectorInfoVO.java
 */
@Getter
@Setter
@ToString
public class CollectorInfoVO extends CommonVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idx;
    private String name;
    private String url;
    private String starthhmm;
    private String repeatss;
    private String regdt;
    private Boolean active;

}