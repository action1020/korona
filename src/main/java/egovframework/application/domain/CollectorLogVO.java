package egovframework.application.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description CollectorLogVO.java
 */
@Getter
@Setter
@ToString
public class CollectorLogVO extends CommonVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long clctSeq;
    private String startDtm;
    private String finishDtm;
    private Integer cnt;

}
