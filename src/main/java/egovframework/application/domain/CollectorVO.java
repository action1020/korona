package egovframework.application.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description CommonVO.java
 */
@Getter
@Setter
@ToString
public class CollectorVO extends CommonVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String regdt;
    private Integer round;
    private Boolean start; // left(true) : right ( false )
    private Boolean odd; // odd(true) : even ( false )
    private Boolean line; // 3 (true) : 4 ( false )
    private String remark;

}