package egovframework.application.domain;

import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description AbstractCommonVO.java
 */
public abstract class AbstractCommonVO {

    private PaginationInfo paginationInfo;

    /**
     * 페이지네이션 정보 클래스를 초기화 하고 검색조건과 페이징 정보를 셋팅한다.
     *
     * @param searchVO
     */
    public void setPage(SearchVO searchVO) {

        paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(searchVO.getPageNo());
        paginationInfo.setRecordCountPerPage(searchVO.getPageSize() > 1000 ? 100 : searchVO.getPageSize()); // 최대 천건까지 수용하고 천건이상은 백건만 보여준다.
        paginationInfo.setPageSize(searchVO.getPageSize());

        searchVO.setPageSize(paginationInfo.getRecordCountPerPage());

    }

    /**
     * Mybatis 에서 VO 대신 egovMap 사용을 위해 전체 카운팅만 셋트하고 페이지인포를 리턴하게 변경한다.
     *
     * @param totalCount
     * @return PaginationInfo
     */
    public PaginationInfo getPaginationInfo(int totalCount) {
        paginationInfo.setTotalRecordCount(totalCount);
        return paginationInfo;
    }

}