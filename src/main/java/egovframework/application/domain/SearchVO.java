package egovframework.application.domain;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 2. 5.
 * @description SearchVO.java
 */
@Getter
@Setter
@ToString
public class SearchVO extends CommonVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private int pageNo = 0;
    /**
     * 페이징
     */
    private int pageSize = 15;
    private int totalCount = 0;
    private int totalPage = 0;
    private int startPage = 0;
    private int endPage = 15;
    private int blockSize = 10;
    private int startBlock = 0;
    private int endBlock = 0;

    private String searchWord;
    /**
     * 검색조건
     */
    private String searchOption;
    private String searchWord2;
    private String searchOption2;
    private String searchWord3;
    private String searchOption3;
    private String searchWord4;
    private String searchOption4;
    private String searchWord5;
    private String searchOption5;
    private String searchWord6;
    private String searchOption6;
    private String searchWord7;
    private String searchOption7;
    private String searchWord8;
    private String searchOption8;
    private String searchWord9;
    private String searchOption9;
    private String searchWord10;
    private String searchOption10;

    private String searchStart;
    /**
     * 기간검색
     */
    private String searchEnd;
    private String searchStart2;
    private String searchEnd2;
    private String searchStart3;
    private String searchEnd3;
    private String searchStart4;
    private String searchEnd4;

    private Integer[] searchIntegerArr;
    /**
     * 인티저 배열 검색
     **/
    private Integer[] searchIntegerArr2;
    private Integer[] searchIntegerArr3;

    public void calculation() {

        this.totalPage = (this.totalCount / this.pageSize) + 1;
        this.startPage = ((this.pageNo > 0 ? this.pageNo : 1) - 1) * this.pageSize;
        this.endPage = this.pageSize;

        if (this.totalCount % this.pageSize == 0) {
            this.totalPage--;
        }

        int totalBlock = (int) Math.ceil(this.totalCount / (double) this.pageSize);
        this.startBlock = ((this.pageNo - 1) / this.blockSize * this.blockSize) + 1;
        this.endBlock = ((this.pageNo - 1) / this.blockSize * this.blockSize) + this.blockSize;

        if (this.endBlock > totalBlock) {
            this.endBlock = totalBlock;
        }
    }

    public EgovMap getPage(List<EgovMap> egovMapList) {

        EgovMap page = new EgovMap();

        if (egovMapList == null || egovMapList.size() < 1) {
            this.setTotalCount(0);
        } else {

            this.setTotalCount(Integer.parseInt(egovMapList.get(0).get("totalcount").toString()));
        }

        this.calculation();

        page.put("currentPage", this.getPageNo());            // 현재 페이지 번호
        page.put("pageSize", this.getPageSize());            // 한 페이지 사이즈
        page.put("totalElements", this.getTotalCount());    // 전체 게시물 수
        page.put("totalPages", this.getTotalPage());        // 전체 페이지 수

        System.out.println("page > " + page );

        return page;
    }

}