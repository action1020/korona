package egovframework.application.collector.service;

import egovframework.application.domain.CollectorVO;
import egovframework.application.domain.SearchVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;

public interface CollectorService {

    /**
     * 게임 데이터 등록
     *
     * @param list
     * @return
     * @throws Exception
     */
    int createGameResult(List<CollectorVO> list) throws Exception;

    /**
     * 게임 누적 데이터 정보 찾기
     *
     * @param round
     * @return
     * @throws Exception
     */
    CollectorVO getTotalResultOddEven(CollectorVO collectorVO) throws Exception;

    /**
     * 게임 패턴 분류하기
     *
     * @param game
     * @throws Exception
     */
    void createPattern(String game) throws Exception;

    /**
     * 게임 종륲
     *
     * @return
     * @throws Exception
     */
    List<EgovMap> getGames() throws Exception;

    /**
     * 마지막 게임 회차를 구하기
     *
     * @param cv
     * @return
     * @throws Exception
     */
    int getLastGame(CollectorVO cv) throws Exception;

    boolean getRandomOddOrEven() throws Exception;

    String getP5(CollectorVO cv) throws Exception;
    String getP6(CollectorVO cv) throws Exception;
    String getP7(CollectorVO cv) throws Exception;
    String getP8(CollectorVO cv) throws Exception;
    String getP9(CollectorVO cv) throws Exception;
    String getP10(CollectorVO cv) throws Exception;
    String getP11(CollectorVO cv) throws Exception;
    String getP12(CollectorVO cv) throws Exception;

    void createGameTip(CollectorVO collectorTipVO) throws Exception;

    void makeGameResult(String game) throws Exception;

    List<EgovMap> retrieveList(SearchVO searchVO) throws Exception;

    List<EgovMap> getSummary(String game) throws Exception;

    EgovMap getCurrentPercent(CollectorVO cv) throws Exception;

    int retrieveLastResult(CollectorVO collectorVO) throws Exception;

    List<CollectorVO> retrieveLastResultOfLimit(CollectorVO cv) throws Exception;

    void cleanTotalGame() throws Exception;
}