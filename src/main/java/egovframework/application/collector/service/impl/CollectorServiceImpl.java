package egovframework.application.collector.service.impl;

import egovframework.application.collector.api.Collector;
import egovframework.application.collector.mapper.CollectorMapper;
import egovframework.application.collector.service.CollectorService;
import egovframework.application.domain.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by action1020s@gmail.com on 2018-02-28
 */
@Slf4j
@Service("collectorService")
public class CollectorServiceImpl extends EgovAbstractServiceImpl implements CollectorService {

    @Resource(name = "propertiesService")
    private EgovPropertyService props;

    @Resource(name = "collectorMapper")
    private CollectorMapper collectorMapper;

    @Resource(name = "sqlSession")
    private SqlSessionFactory sqlSessionFactory;

    /**
     * 게임 데이터 등록
     *
     * @param list
     * @return
     * @throws Exception
     */
    @Override
    public int createGameResult(List<CollectorVO> list) throws Exception {
        int result = 0;

        for ( CollectorVO vo : list ) {

            int count = collectorMapper.retriveHasGameResult(vo);
            if( count == 0){
                result += collectorMapper.createGameResult(vo);
            }

        }

        return result;

    }

    /**
     * 게임 누적 데이터 정보 찾기
     *
     * @param round
     * @return
     * @throws Exception
     */
    @Override
    public CollectorVO getTotalResultOddEven(CollectorVO collectorVO) throws Exception {
        return collectorMapper.retrieveTotalResultOddEven(collectorVO);
    }

    /**
     * 게임 패턴 분리하기
     *
     * @param game
     * @throws Exception
     */
    @Override
    public void createPattern(String game) throws Exception {

        List<CollectorVO> collectorVOList = collectorMapper.retrieveGameAllData(game);

        if ( collectorVOList.size() < 13){
            log.debug("["+ game +"] 패턴을 만들지 않습니다.");
            return;
        }

        String pattern = ""
                , p5 = "", p5n = ""
                , p6 = "", p6n = ""
                , p7 = "", p7n = ""
                , p8 = "", p8n = ""
                , p9 = "", p9n = ""
                , p10 = "", p10n = ""
                , p11 = "", p11n = ""
                , p12 = "", p12n = ""
                , pName = "";

        int count = 0;

        for( CollectorVO collectorVo : collectorVOList ){

            count ++ ;
            pName = "";

            if ( collectorVo.getStart().equals(true) ){
                pName = pName.concat("좌");
            }else{
                pName = pName.concat("우");
            }

            if ( collectorVo.getLine().equals(true) ){
                pName = pName.concat("3");
            }else{
                pName = pName.concat("4");
            }

            if( collectorVo.getOdd().equals(true) ){
                pName = pName.concat("홀");
            }else{
                pName = pName.concat("짝");
            }

            pattern = pattern.concat(pName);

            if ( count == 5 ){
                p5 = pattern;
            }
            if ( count == 6 ){
                p5n = pName;
                p6 = pattern;
            }

            if ( count == 7 ){
                p6n = pName;
                p7 = pattern;
            }

            if ( count == 8 ){
                p7n = pName;
                p8 = pattern;
            }
            if ( count == 9 ){
                p8n = pName;
                p9 = pattern;
            }
            if ( count == 10){
                p9n = pName;
                p10 = pattern;

            }
            if( count == 11){
                p10n = pName;
                p11 = pattern;
            }
            if( count == 12){
                p11n = pName;
                p12 = pattern;
            }
            if ( count == 13 ){
                p12n = pName;
                HashMap hash = new HashMap();
                hash.put("name", collectorVo.getName());
                hash.put("p5", p5);
                hash.put("p6", p6);
                hash.put("p7", p7);
                hash.put("p8", p8);
                hash.put("p9", p9);
                hash.put("p10", p10);
                hash.put("p11", p11);
                hash.put("p12", p12);
                hash.put("p5n", p5n);
                hash.put("p6n", p6n);
                hash.put("p7n", p7n);
                hash.put("p8n", p8n);
                hash.put("p9n", p9n);
                hash.put("p10n", p10n);
                hash.put("p11n", p11n);
                hash.put("p12n", p12n);
                collectorMapper.createPattern(hash);
            }
        }



    }

    /**
     * 랜덤 홀짝 반환
     *
     * @return
     */
    public boolean getRandomOddOrEven(){
        Random r = new Random();
        return  r.nextInt(100) % 2 == 0 ? false : true;
    }

    /**
     * 등록된 게임 목록 구함
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<EgovMap> getGames() throws Exception {
        return collectorMapper.getGames();
    }

    /**
     * 마지막 회차 구하기
     * @param cv
     * @return
     * @throws Exception
     */
    @Override
    public int getLastGame(CollectorVO cv) throws Exception {
        return collectorMapper.getLastGame(cv);
    }

    @Override
    public String getP5(CollectorVO cv) throws Exception {
        return collectorMapper.getP5(cv);
    }

    @Override
    public String getP6(CollectorVO cv) throws Exception {
        return collectorMapper.getP6(cv);
    }

    @Override
    public String getP7(CollectorVO cv) throws Exception {
        return collectorMapper.getP7(cv);
    }

    @Override
    public String getP8(CollectorVO cv) throws Exception {
        return collectorMapper.getP8(cv);
    }

    @Override
    public String getP9(CollectorVO cv) throws Exception {
        return collectorMapper.getP10(cv);
    }

    @Override
    public String getP10(CollectorVO cv) throws Exception {
        return collectorMapper.getP10(cv);
    }

    @Override
    public String getP11(CollectorVO cv) throws Exception {
        return collectorMapper.getP11(cv);
    }

    @Override
    public String getP12(CollectorVO cv) throws Exception {
        return collectorMapper.getP12(cv);
    }

    @Override
    public void createGameTip(CollectorVO collectorTipVO) throws Exception {
        int round = collectorMapper.retrieveGameTip(collectorTipVO);

        if( round == 0){
            collectorMapper.createGameTip(collectorTipVO);
        }
    }

    @Override
    public void makeGameResult(String game) throws Exception {
        CollectorVO cv = new CollectorVO();
        cv.setName(game);
        collectorMapper.makeGameResult(cv);
    }

    @Override
    public List<EgovMap> retrieveList(SearchVO searchVO) throws Exception{
        return collectorMapper.retrieveList(searchVO);
    }

    @Override
    public List<EgovMap> getSummary(String game) throws Exception {
        return collectorMapper.getSummary(game);
    }

    @Override
    public EgovMap getCurrentPercent(CollectorVO cv) throws Exception {
        return collectorMapper.getCurrentPercent(cv);
    }

    @Override
    public int retrieveLastResult(CollectorVO cv) throws Exception {
        return collectorMapper.retrieveLastResult(cv);
    }

    @Override
    public List<CollectorVO> retrieveLastResultOfLimit(CollectorVO cv) throws Exception {
        return collectorMapper.retrieveLastResultOfLimit(cv);
    }

    @Override
    public void cleanTotalGame() throws Exception {
        collectorMapper.cleanTotalGame();
        collectorMapper.cleanResultGame();
    }
}