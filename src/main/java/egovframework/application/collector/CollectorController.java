package egovframework.application.collector;

import egovframework.application.collector.service.CollectorService;
import egovframework.rte.fdl.property.EgovPropertyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by action1020s@gmail.com on 2018-02-26
 */
@Slf4j
@Controller
@RequestMapping(value = "/collector/")
public class CollectorController {

    @Resource(name = "propertiesService")
    private EgovPropertyService props;

    @Resource(name = "collectorService")
    private CollectorService collectorService;

    /**
     * retrieveList
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "collectorList")
    public String retrieveList(ModelMap model) {
        log.debug("list");
        return "/collector/collectorList";
    }
}