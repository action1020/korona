package egovframework.application.collector.mapper;

import egovframework.application.domain.*;
import egovframework.rte.psl.dataaccess.mapper.Mapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import org.apache.tools.ant.taskdefs.EchoXML;

import java.util.HashMap;
import java.util.List;

/**
 * Created by action1020s@gmail.com on 2018-02-28
 */
@Mapper(value = "collectorMapper")
public interface CollectorMapper {

    /**
     * retrieveCollectorInfo
     *
     * @param collectorInfoVO
     * @return
     * @throws Exception
     */
    CollectorInfoVO retrieveCollectorInfo(CollectorInfoVO collectorInfoVO) throws Exception;

    /**
     * retriveCollecterInfoToHashCheck
     *
     * @param collectorInfoVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retriveCollecterInfoToHashCheck(CollectorInfoVO collectorInfoVO) throws Exception;

    /**
     * changeToCollectorActiveStatus
     *
     * @param collectorInfoVO
     * @throws Exception
     */
    void changeToCollectorActiveStatus(CollectorInfoVO collectorInfoVO) throws Exception;

    /**
     * retrieveExistHash
     *
     * @param collectorVO
     * @return
     * @throws Exception
     */
    int countOfCollectorHash(CollectorVO collectorVO) throws Exception;

    /**
     * createCollectorArticle
     *
     * @param collectorVO
     * @throws Exception
     */
    void createCollectorArticle(CollectorVO collectorVO) throws Exception;

    /**
     * createCollectorHash
     *
     * @param collectorVO
     * @throws Exception
     */
    void createCollectorHash(CollectorVO collectorVO) throws Exception;


    /**
     * createCollectorLink
     *
     * @param collectorLinkVO
     * @throws Exception
     */
    void createCollectorLink(CollectorLinkVO collectorLinkVO) throws Exception;

    /**
     * createCollectorLog
     *
     * @param collectorLogVO
     * @throws Exception
     */
    void createCollectorLog(CollectorLogVO collectorLogVO) throws Exception;



    /**
     * retrieveExportDataList
     *
     * @param collectorVO
     * @return
     */
    List<EgovMap> retrieveExportDataList(CollectorVO collectorVO) throws Exception;

    /**
     * retrieveTargetCollector
     *
     * @return
     * @throws Exception
     */
    List<CollectorInfoVO> retrieveTargetCollector() throws Exception;

    /**
     * retriveCollectorList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retrieveCollectorList(SearchVO searchVO) throws Exception;

    /**
     * createCollectorInfo
     *
     * @param collectorInfoVO
     * @throws Exception
     */
    void createCollectorInfo(CollectorInfoVO collectorInfoVO) throws Exception;

    /**
     * updateCollectorInfo
     *
     * @param collectorInfoVO
     * @throws Exception
     */
    void updateCollectorInfo(CollectorInfoVO collectorInfoVO) throws Exception;

    /**
     * retriveCollectorHisList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retriveCollectorHisList(SearchVO searchVO) throws Exception;

    /**
     * retriveCollectorDtaList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retriveCollectorDtaList(SearchVO searchVO) throws Exception;

    /**
     * retriveCollectorDtaAttachList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retriveCollectorDtaAttachList(SearchVO searchVO) throws Exception;

    /**
     * retriveCollectorDtaLinkList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retriveCollectorDtaLinkList(SearchVO searchVO) throws Exception;

    /**
     * retrieveFile
     *
     * @param searchVO
     * @return
     */
    EgovMap retrieveFile(SearchVO searchVO) throws Exception;

    /**
     * retrievePdfFile
     *
     * @param searchVO
     * @return
     */
    EgovMap retrievePdfFile(SearchVO searchVO) throws Exception;

    int retriveHasGameResult(CollectorVO vo) throws Exception;

    int createGameResult(CollectorVO vo) throws Exception;

    /**
     * retrieveTotalResultOddEven
     * @param round
     * @return
     */
    CollectorVO retrieveTotalResultOddEven(CollectorVO collectorVO) throws Exception;

    /**
     * retriveGameAllData
     * @param game
     * @return
     */
    List<CollectorVO> retrieveGameAllData(String game) throws Exception;

    /**
     * createPattern
     * @param hash
     */
    void createPattern(HashMap hash) throws Exception;

    /**
     * getGames
     *
     * @return
     */
    List<EgovMap> getGames() throws Exception;

    /**
     * getLastGame
     *
     * @param cv
     * @return
     * @throws Exception
     */
    int getLastGame(CollectorVO cv) throws Exception;

    /**
     * get patterns
     *
     * @param cv
     * @return
     * @throws Exception
     */
    String getP5(CollectorVO cv) throws Exception;
    String getP6(CollectorVO cv) throws Exception;
    String getP7(CollectorVO cv) throws Exception;
    String getP8(CollectorVO cv) throws Exception;
    String getP9(CollectorVO cv) throws Exception;
    String getP10(CollectorVO cv) throws Exception;
    String getP11(CollectorVO cv) throws Exception;
    String getP12(CollectorVO cv) throws Exception;

    int retrieveGameTip(CollectorVO collectorTipVO) throws Exception;

    void createGameTip(CollectorVO collectorTipVO) throws Exception;

    void makeGameResult(CollectorVO collectorVO) throws Exception;

    List<EgovMap> retrieveList(SearchVO searchVO) throws Exception;

    List<EgovMap> getSummary(String game) throws Exception;

    EgovMap getCurrentPercent(CollectorVO cv) throws Exception;

    int retrieveLastResult(CollectorVO cv) throws Exception;

    List<CollectorVO> retrieveLastResultOfLimit(CollectorVO cv) throws Exception;

    void cleanTotalGame() throws Exception;

    void cleanResultGame() throws Exception;
}