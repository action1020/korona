package egovframework.application.collector;

import egovframework.application.collector.collectors.*;
import egovframework.application.domain.CollectorVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by action1020s@gmail.com on 2018-03-06
 */
public class CollectorMain {


    public static void main(String ar[]) throws Exception {

        Map m = new HashMap();



        for ( int i = 1 ; i <= 1000; i++ ) {

            String rate = String.format("%.2f", getRate() * 100.0f);

            if( null == m.get( rate ) ){
                m.put(rate, 1);
            }else{
                int rs = (Integer) m.get(rate);
                m.put(rate, ++rs);
            }

           // System.out.print("\t\t" + rate + "\t");

            //if( i % 10 == 0){
             //   System.out.println();
            //}

        }

        System.out.println(m);

    }

    public static double getRate(){

        double[] rate = {
                0.5005 , // 1 꺽
                0.2505 , // 2
                0.1805 , // 3
                0.15   , // 4
                0.0583 , // 5
                0.0267 , // 6 줄 구간
                0.0233 , // 7
                0.02   , // 8
                0.0075 , // 9
                0.001    // 10
        };

        double r = ThreadLocalRandom.current().nextDouble(0, 1);

        if (r <= rate[9]) {
            return rate[9];
        }else if (r <= rate[8]) {
            return rate[8];
        }else if (r <= rate[7]) {
            return rate[7];
        }else if (r <= rate[6]) {
            return rate[6];
        }else if (r <= rate[5]) {
            return rate[5];
        }else if (r <= rate[4]) {
            return rate[4];
        }else if (r <= rate[3]) {
            return rate[3];
        }else if (r <= rate[2]) {
            return rate[2];
        }else if (r <= rate[1]) {
            return rate[1];
        }else if (r <= rate[0]) {
            return rate[0];
        }else{
            return getRate();
        }

    }

}
