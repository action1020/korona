package egovframework.application.collector.api;

import egovframework.application.domain.CollectorInfoVO;
import egovframework.application.domain.CollectorVO;

import java.util.List;

/**
 * Created by action1020s@gmail.com on 2018-02-27
 */
public interface CollectorJob {

    List<CollectorVO> start(CollectorInfoVO collectorInfoVO) throws Exception;

}