package egovframework.application.collector.api;

/**
 * Created by action1020s@gmail.com on 2018-02-27
 */
public class CollectorException extends RuntimeException {

    public CollectorException(String message) {
        super(message);
    }
}