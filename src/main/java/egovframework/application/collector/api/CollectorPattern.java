package egovframework.application.collector.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by action1020s@gmail.com on 2018-03-07
 */
public class CollectorPattern {

    /**
     * parseRegexToDateFormat
     * <p>
     * Parse Date TimeString (2018년10월20일/ 2018年 10月 20日)
     *
     * @param str
     * @return
     */
    public static String parseRegexToDateFormat(String str) {

        String pattern = "(\\d{4})+.\\s*(\\d{1,2})+.\\s*(\\d{1,2})";
        String ymd = null;

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(str);

        if (m.find()) {

            String yyyy = m.group(1);
            String MM = m.group(2);
            String dd = m.group(3);

            try {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                ymd = sdf.format(sdf.parse(yyyy.concat("-").concat(MM).concat("-").concat(dd)));

            } catch (ParseException e) {
            } catch (Exception e) {
            }
        }

        return ymd;
    }

    /**
     * parseRegexToYear
     * <p>
     * https://blog.naver.com/action1020/{{year}} --> https://blog.naver.com/action1020/2018
     * yyyy To 2018
     *
     * @param URI
     * @return
     */
    public static String parseRegexToYear(String URI) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return URI.replace("{{year}}", sdf.format(Calendar.getInstance().getTime()));

    }

    /**
     * parseRegexToMonth
     * <p>
     * http://www.cidrap.umn.edu/news-perspective/2018/{{MM}}/news-scan-mar-15-2018
     * {MM} To 03
     *
     * @param URI
     * @return
     */
    public static String parseRegexToMonth(String URI) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        return URI.replace("{{MM}}", sdf.format(Calendar.getInstance().getTime()));
    }

    /**
     * parseRegexToMonthForMMM
     * <p>
     * http://www.cidrap.umn.edu/news-perspective/2018/03/news-scan-{MMM}-15-2018
     * {MMM} To Mar
     *
     * @param URI
     * @return
     */
    public static String parseRegexToMonthForMMM(String URI) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.ENGLISH);
        return URI.replace("{{MMM}}", sdf.format(Calendar.getInstance().getTime()));
    }

    /**
     * parseRegexToMonthForMMMToLower
     * http://www.cidrap.umn.edu/news-perspective/2018/03/news-scan-{mmm}-15-2018
     * {mmm} To mar
     *
     * @param URI
     * @return
     */
    public static String parseRegexToMonthForMMMToLower(String URI) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.ENGLISH);
        return URI.replace("{{mmm}}", sdf.format(Calendar.getInstance().getTime()).toLowerCase());
    }

    /**
     * parseRegexToDay
     * <p>
     * http://www.cidrap.umn.edu/news-perspective/2018/03/news-scan-mar-{dd}-2018
     * {dd} To 14
     *
     * @param URI
     * @return
     */
    public static String parseRegexToDay(String URI) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        return URI.replace("{{dd}}", sdf.format(Calendar.getInstance().getTime()));
    }

    /**
     * parseRegexToDomain
     * <p>
     * http://blog.naver.com/action1020 -> http://blog.naver.com/
     * http://blog.naver.com:80/action1020 -> http://blog.naver.com:80/
     *
     * @param URL
     * @return
     */
    public static String parseRegexToDomain(String URL) {

        String pattern = "^((https?):\\/)?\\/?([^:\\/\\s]+)(:\\d*)?(\\/)?";
        String ymd = null;

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(URL);

        if (m.find()) {
            URL = m.group();
        }

        return URL;
    }

}
