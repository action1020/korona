package egovframework.application.collector.api;

/**
 * Created by action1020s@gmail.com on 2018-02-27
 */
public class CollectorRunningException extends RuntimeException {

    public CollectorRunningException(String message) {
        super(message);
    }
}