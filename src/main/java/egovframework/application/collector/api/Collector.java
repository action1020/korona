package egovframework.application.collector.api;

import egovframework.application.domain.CollectorInfoVO;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by action1020s@gmail.com on 2018-02-27
 */
@Slf4j
public abstract class Collector implements CollectorJob {

    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0";
    public final static String JSOUP_REF = "http://www.google.com";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public List<CollectorVO> collectorVOList = null;
    private boolean status = false;
    private int checkCountDuplicateHash = 0;

    /**
     * Collector instance status Checked
     */
    public void doStartCollector() {
        this.status = true;
    }

    public void doEndCollector() {
        this.status = false;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void init() {

        collectorVOList = new ArrayList<CollectorVO>();
        checkCountDuplicateHash = 0;
    }

    /**
     * return JSOUP Document Object
     *
     * @param URL
     * @return
     */
    public Document getDocumentFromURL(String URL) throws Exception {
        return getDocumentFromURL(URL, JSOUP_AGENT, JSOUP_REF, JSOUP_TIMEOUT);
    }

    public Document getDocumentFromURL(String URL, String AGENT) throws Exception {
        return getDocumentFromURL(URL, AGENT, JSOUP_REF, JSOUP_TIMEOUT);
    }

    public Document getDocumentFromURL(String URL, String AGENT, String REF) throws Exception {
        return getDocumentFromURL(URL, AGENT, REF, JSOUP_TIMEOUT);
    }

    public Document getDocumentFromURL(String URL, String AGENT, String REF, Integer TIMEOUT) throws Exception {
            Connection.Response response = Jsoup.connect(URL).ignoreContentType(true).userAgent(AGENT).referrer(REF).timeout(TIMEOUT).maxBodySize(0).
                    followRedirects(true).validateTLSCertificates(false).execute();
            return response.parse();
    }

    /**
     * return Web Resource To Inputstream
     *
     * @param URL
     * @return
     * @throws Exception
     */
    public byte[] getResourceFromURL(String URL) throws Exception {
        return getResourceFromURL(URL, JSOUP_AGENT, JSOUP_REF, JSOUP_FILE_TIMEOUT);
    }

    public byte[] getResourceFromURL(String URL, String AGENT) throws Exception {
        return getResourceFromURL(URL, AGENT, JSOUP_REF, JSOUP_FILE_TIMEOUT);
    }

    public byte[] getResourceFromURL(String URL, String AGENT, String REF) throws Exception {
        return getResourceFromURL(URL, AGENT, REF, JSOUP_FILE_TIMEOUT);
    }

    public byte[] getResourceFromURL(String URL, String AGENT, String REF, Integer TIMEOUT) throws Exception {

        byte[] bytes = null;

        try {

            // first JSOUP FILE DOWNLOAD
            Connection.Response response = Jsoup.connect(URL).ignoreContentType(true).userAgent(AGENT).referrer(REF).timeout(TIMEOUT).
                    followRedirects(true).maxBodySize(0).validateTLSCertificates(false).execute();
            bytes = response.bodyAsBytes();

        } catch (Exception e) {

            // second JAVA URL
            java.net.URL url = new URL(URL);
            InputStream in = url.openStream();

            if (null != in) {

                byte[] buffer = new byte[1024];
                ByteArrayOutputStream output = new ByteArrayOutputStream();

                try {
                    int numRead = 0;
                    while ((numRead = in.read(buffer)) > -1) {
                        output.write(buffer, 0, numRead);
                    }
                } catch (Exception ex2) {
                    throw ex2;
                } finally {
                    in.close();
                }

                output.flush();
                bytes = output.toByteArray();

            }

            // Exception throw
            if (null == bytes) {
                throw e;
            }


        }

        return bytes;
    }

    /**
     * getResourceNameFromURL
     *
     * @param URL
     * @return
     * @throws Exception
     */
    public String getResourceNameFromURL(String URL) throws Exception {
        return URL.substring(URL.lastIndexOf("/") + 1);
    }

    /**
     * getExtensionFromString
     *
     * @param FILE
     * @return
     * @throws Exception
     */
    public String getExtensionFromString(String FILE) throws Exception {
        return FILE.substring(FILE.lastIndexOf(".") + 1);
    }

    /**
     * return to MD5 Hash (32)
     *
     * @param str
     * @return
     * @throws Exception
     */
    public String getHash(String str) throws Exception {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes());

        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();

    }

    /**
     * getRegularChangeURL
     *
     * @param URL
     * @return
     */
    public String getRegxpURL(String URL) {

        String pattern = "(\\{\\{[a-zA-Z]+\\}\\})";

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(URL);

        while (m.find()) {

            if ("{{year}}".equals(m.group(1))) {
                URL = CollectorPattern.parseRegexToYear(URL);
            }

            if ("{{MM}}".equals(m.group(1))) {
                URL = CollectorPattern.parseRegexToMonth(URL);
            }

            if ("{{dd}}".equals(m.group(1))) {
                URL = CollectorPattern.parseRegexToDay(URL);
            }

            if ("{{MMM}}".equals(m.group(1))) {
                URL = CollectorPattern.parseRegexToMonthForMMM(URL);
            }

            if ("{{mmm}}".equals(m.group(1))) {
                URL = CollectorPattern.parseRegexToMonthForMMMToLower(URL);
            }
        }

        return URL;
    }

}