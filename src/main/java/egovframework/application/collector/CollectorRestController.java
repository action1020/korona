package egovframework.application.collector;

import egovframework.application.collector.api.CollectorException;
import egovframework.application.collector.api.CollectorRunningException;
import egovframework.application.collector.collectors.Collector_Mario1M;
import egovframework.application.collector.service.CollectorService;
import egovframework.application.domain.CollectorInfoVO;
import egovframework.application.domain.CollectorVO;
import egovframework.application.domain.SearchVO;
import egovframework.framework.contants.Ajax;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 3. 23.
 * @description CollectorRestController.java
 */
@Slf4j
@RestController
@RequestMapping(value = "/collector/rest/", produces = "application/json", method = RequestMethod.POST)
public class CollectorRestController {

    @Resource(name = "collectorService")
    private CollectorService collectorService;

    @Resource(name = "collectorScheduler")
    private CollectorScheduler collectorScheduler;

    @RequestMapping(value = "retrieveCollectorList")
    public EgovMap retrieveList(SearchVO searchVO) {

        log.debug("retrieveList");
        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = collectorService.retrieveList(searchVO);

            res.put("page", searchVO.getPage(list));
            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    @RequestMapping(value = "getSummary")
    public EgovMap getSummary(SearchVO searchVO) {

        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = collectorService.getSummary(StringUtils.isEmpty(searchVO.getSearchOption()) ? null : searchVO.getSearchOption());

            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;

    }

    @RequestMapping(value = "createTips")
    public EgovMap createTips(SearchVO searchVO) {

        EgovMap res = new EgovMap();

        try {

            if( "Bubble2_1m".equals( searchVO.getSearchOption())){
                collectorScheduler.Bubble2_1m();
            } else if( "Mario1m".equals( searchVO.getSearchOption())){
                collectorScheduler.Mario1m();
            } else if( "Mario2m".equals( searchVO.getSearchOption())){
                collectorScheduler.Mario2m();
            } else if( "Mario3m".equals( searchVO.getSearchOption())){
                collectorScheduler.Mario3m();
            } else if( "Star1m".equals( searchVO.getSearchOption())){
                collectorScheduler.Star1m();
            } else if( "Star2m".equals( searchVO.getSearchOption())){
                collectorScheduler.Star2m();
            } else if( "Star3m".equals( searchVO.getSearchOption())){
                collectorScheduler.Star3m();
            }

            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;

    }
}