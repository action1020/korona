package egovframework.application.collector;

import egovframework.application.collector.api.Collector;
import egovframework.application.collector.collectors.*;
import egovframework.application.collector.service.CollectorService;
import egovframework.application.domain.CollectorVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by action1020s@gmail.com on 2018-03-22
 */
@Component("collectorScheduler")
@Slf4j
public class CollectorScheduler {

    @Resource(name = "propertiesService")
    private EgovPropertyService props;

    @Resource(name = "collectorService")
    private CollectorService collectorService;

    /**
     * 마리오 1분
     *
     * @throws Exception
     */
    public void Mario1m() throws Exception {

        Collector_Mario1M collector = Collector_Mario1M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Mario1m");
        MakeGameTip("Mario1m");
//        makePattern("Mario1m");

    }

    /**
     * 마리오 2분
     *
     * @throws Exception
     */
    public void Mario2m() throws Exception {

        Collector_Mario2M collector = Collector_Mario2M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Mario2m");
        MakeGameTip("Mario2m");
//        makePattern("Mario2m");

    }

    /**
     * 마리오 3분
     *
     * @throws Exception
     */
    public void Mario3m() throws Exception {

        Collector_Mario3M collector = Collector_Mario3M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Mario3m");
        MakeGameTip("Mario3m");
//        makePattern("Mario3m");

    }

    /**
     * 별다리 1분
     *
     * @throws Exception
     */
    public void Star1m() throws Exception {

        Collector_Star1M collector = Collector_Star1M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Star1m");
        MakeGameTip("Star1m");
//        makePattern("Star1m");

    }

    /**
     * 별다리 2분
     *
     * @throws Exception
     */
    public void Star2m() throws Exception {

        Collector_Star2M collector = Collector_Star2M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Star2m");
        MakeGameTip("Star2m");
//        makePattern("Star2m");

    }

    /**
     * 별다리 3분
     *
     * @throws Exception
     */
    public void Star3m() throws Exception {

        Collector_Star3M collector = Collector_Star3M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Star3m");
        MakeGameTip("Star3m");
//        makePattern("Star3m");

    }

    /**
     * 뽀글이 1분
     *
     * @throws Exception
     */
    public void Bubble1m() throws Exception {

        Collector_Bubble1M collector = Collector_Bubble1M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Bubble1m");
        MakeGameTip("Bubble1m");
//        makePattern("Bubble1m");
    }

    /**
     * 뽀글이 2분
     *
     * @throws Exception
     */
    public void Bubble2m() throws Exception {

        Collector_Bubble2M collector = Collector_Bubble2M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Bubble2m");
        MakeGameTip("Bubble2m");
//        makePattern("Bubble2m");
    }

    /**
     * 뽀글이 3분
     *
     * @throws Exception
     */
    public void Bubble3m() throws Exception {

        Collector_Bubble3M collector = Collector_Bubble3M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Bubble3m");
        MakeGameTip("Bubble3m");
//        makePattern("Bubble3m");

    }

    /**
     * 벌크 인서트
     *
     * @throws Exception
     */
    public void Bubble_Bulk() throws Exception {

        Collector_Bubble1_Bulk collector = Collector_Bubble1_Bulk.getInstance();

        String sourceDate = "20200306";
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date myDate = format.parse(sourceDate);

        for ( int i = 0 ; i< 2 ;i ++) {

            List<CollectorVO> collectList = collector.start(format.format(myDate));
            collectorService.createGameResult(collectList);
            myDate = addDays(myDate, 1);

        }

        System.out.println( "END DATE ---> "  + format.format(myDate));

    }

    /**
     * 버블 1분
     *
     * @throws Exception
     */
    public void Bubble2_1m() throws Exception {

        Collector_Bubble2_1M collector = Collector_Bubble2_1M.getInstance();
        List<CollectorVO> collectList = collector.start();
        collectorService.createGameResult(collectList);

        MakeGameResult("Bubble2_1m");
        MakeGameTip("Bubble2_1m");
//        makePattern("Bubble2_1m");
    }

    /**
     * 게임 수집 결과 데이터터는 삭제
     *
     * @throws Exception
     */
    public void TotalGame_Clean() throws Exception {
        collectorService.cleanTotalGame();
   }

    /**
     * 패턴 분류
     *
     * @throws Exception
     */
    public void Pattern() throws Exception {

        List<EgovMap> egovMap = collectorService.getGames();

        for( EgovMap m : egovMap ){
            collectorService.createPattern((String) m.get("nick"));
        }
    }

    /**
     * 예상 결과 적중 여부
     *
     * @param game
     * @throws Exception
     */
    public void MakeGameResult(String game) throws Exception {
        collectorService.makeGameResult(game);
    }

    /**
     * 예상 결과 만들기 ( 1차 버전)
     *
     * @throws Exception
     *
    public void MakeGameTip(String game) throws Exception {

        int odd = 0, even = 0;
        int l3 = 0, l4 = 0;
        int left = 0, right = 0;
        String remark = "";

        CollectorVO cv = new CollectorVO();
        cv.setName(game);

        int lastRound = collectorService.getLastGame(cv);

        if ( (game.equals("Bubble1m") && lastRound == 1200 ) || (game.equals("Star1m") && lastRound == 1440 ) || (game.equals("Mario1m") && lastRound == 1200 ) || (game.equals("Bubble2_1m") && lastRound == 1440 ) ){
            lastRound = 1;
        }else if ( (game.equals("Mario2m") && lastRound == 720 ) || (game.equals("Bubble2m") && lastRound == 720 ) || (game.equals("Bubble2m") && lastRound == 720 ) ){
            lastRound = 1;
        }else if ( (game.equals("Bubble3m") && lastRound == 480 ) || (game.equals("Mario3m") && lastRound == 480 ) || (game.equals("Star3m") && lastRound == 480 ) ){
            lastRound = 1;
        }else {
            lastRound = lastRound + 1;
        }

        cv.setRound(lastRound);
        CollectorVO collectorVO = collectorService.getTotalResultOddEven(cv);

        if ( collectorVO.getStart().equals(true) ){
            remark = remark.concat("전체회차 : 좌");
            left++;
        }else{
            remark = remark.concat("전체회차 : 우");
            right++;
        }

        if ( collectorVO.getLine().equals(true)){
            remark = remark.concat(",3");
            l3++;
        }else{
            remark = remark.concat(",4");
            l4++;
        }

        if ( collectorVO.getOdd().equals(true)){
            remark = remark.concat(",홀");
            odd++;
        }else{
            remark = remark.concat(",짝");
            even++;
        }

        String p5n = collectorService.getP5(cv);
        String p6n = collectorService.getP6(cv);
        String p7n = collectorService.getP7(cv);
        String p8n = collectorService.getP8(cv);
        String p9n = collectorService.getP9(cv);
        String p10n = collectorService.getP10(cv);
        String p11n = collectorService.getP11(cv);
        String p12n = collectorService.getP12(cv);

        if ( StringUtils.isNotEmpty(p5n)){
            remark = remark.concat(", p5n : " + p5n);
            left = addPointLeft(p5n, left, 1);
            right = addPointRight(p5n, right, 1);
            l3 = addPointL3(p5n, l3, 1);
            l4 = addPointL4(p5n, l4, 1);
            odd = addPointOdd(p5n, odd, 1);
            even = addPointEven(p5n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p6n)) {
            remark = remark.concat(", p6n : " + p6n);
            left = addPointLeft(p6n, left, 1);
            right = addPointRight(p6n, right, 1);
            l3 = addPointL3(p6n, l3, 1);
            l4 = addPointL4(p6n, l4, 1);
            odd = addPointOdd(p6n, odd, 1);
            even = addPointEven(p6n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p7n)) {
            remark = remark.concat(", p7n : " + p7n);
            left = addPointLeft(p7n, left, 1);
            right = addPointRight(p7n, right, 1);
            l3 = addPointL3(p7n, l3, 1);
            l4 = addPointL4(p7n, l4, 1);
            odd = addPointOdd(p7n, odd, 1);
            even = addPointEven(p7n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p8n)) {
            remark = remark.concat(", p8n : " + p8n);
            left = addPointLeft(p8n, left, 1);
            right = addPointRight(p8n, right, 1);
            l3 = addPointL3(p8n, l3, 1);
            l4 = addPointL4(p8n, l4, 1);
            odd = addPointOdd(p8n, odd, 1);
            even = addPointEven(p8n, even, 1);

        }

        if ( StringUtils.isNotEmpty(p9n)) {
            remark = remark.concat(", p9n : " + p9n);
            left = addPointLeft(p9n, left, 1);
            right = addPointRight(p9n, right, 1);
            l3 = addPointL3(p9n, l3, 1);
            l4 = addPointL4(p9n, l4, 1);
            odd = addPointOdd(p9n, odd, 1);
            even = addPointEven(p9n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p10n)) {
            remark = remark.concat(", p10n : " + p10n);
            left = addPointLeft(p10n, left, 1);
            right = addPointRight(p10n, right, 1);
            l3 = addPointL3(p10n, l3, 1);
            l4 = addPointL4(p10n, l4, 1);
            odd = addPointOdd(p10n, odd, 1);
            even = addPointEven(p10n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p11n)) {
            remark = remark.concat(", p11 : " + p11n);
            left = addPointLeft(p11n, left, 1);
            right = addPointRight(p11n, right, 1);
            l3 = addPointL3(p11n, l3, 1);
            l4 = addPointL4(p11n, l4, 1);
            odd = addPointOdd(p11n, odd, 1);
            even = addPointEven(p11n, even, 1);
        }

        if ( StringUtils.isNotEmpty(p12n)) {
            remark = remark.concat(", p12n : " + p12n);
            left = addPointLeft(p12n, left, 1);
            right = addPointRight(p12n, right, 1);
            l3 = addPointL3(p12n, l3, 1);
            l4 = addPointL4(p12n, l4, 1);
            odd = addPointOdd(p12n, odd, 1);
            even = addPointEven(p12n, even, 1);
        }

        EgovMap eMap =collectorService.getCurrentPercent(cv);
        BigDecimal oddf = (BigDecimal) eMap.get("odd");
        BigDecimal evenf = (BigDecimal) eMap.get("even");

        remark = remark.concat(", 홀 : " + oddf + "% 짝:" + evenf + "%");


        if ( Integer.parseInt(oddf + "") < 46 ){
            odd++;
        }

        if ( Integer.parseInt(evenf + "") < 46 ){
            even++;
        }

        if ( Integer.parseInt(oddf + "") < 45 ){
            odd++;
        }

        if ( Integer.parseInt(evenf + "") < 45 ){
            even++;
        }

        if ( Integer.parseInt(oddf + "") < 44 ){
            odd++;
        }

        if ( Integer.parseInt(evenf + "") < 44 ){
            even++;
        }



        if( odd == even ){
            if ( collectorVO.getOdd().equals(true)){
                odd++;
            }else{
                even++;
            }
        }

        remark = remark.concat(", 우 : " + right + ", 좌 : " + left);
        remark = remark.concat(", 3  : " + l3 + ", 4 : " + l4);
        remark = remark.concat(", 홀 : " + odd + ", 짝 : " + even);

        int lastResult = collectorService.retrieveLastResult(cv);

        Random r = new Random();
        int x = r.nextInt(10);


        if( lastResult == 0 && x < 9 ){
            int _odd = odd;
            int _even = even;

            if( odd > even ){
                _odd = 0;
                _even = 1;
            }else if( even > odd ){
                _odd = 1;
                _even = 0;
            }

            odd = _odd;
            even = _even;

            remark = remark.concat("전회차 미적으로 인해 결과 변경");
        }

        CollectorVO collectorTipVO = new CollectorVO();
        collectorTipVO.setName(cv.getName());
        collectorTipVO.setRound(lastRound);
        collectorTipVO.setLine(l3 > l4);
        collectorTipVO.setStart(left > right);
        collectorTipVO.setOdd(odd > even);
        collectorTipVO.setRemark(remark);

        collectorService.createGameTip(collectorTipVO);

    }

    */

    /**
     * 예상 결과 만들기 ( 2차 버전)
     *
     * @param game
     * @throws Exception
     */
    public void MakeGameTip(String game) throws Exception {

        int odd = 0, even = 0;
        int l3 = 0, l4 = 0;
        int left = 0, right = 0;

        String reverse = "29.99";
        String remark = "";

        CollectorVO cv = new CollectorVO();
        cv.setName(game);

        int lastRound = collectorService.getLastGame(cv);

        if ( (game.equals("Bubble1m") && lastRound == 1200 ) || (game.equals("Star1m") && lastRound == 1440 ) || (game.equals("Mario1m") && lastRound == 1200 ) || (game.equals("Bubble2_1m") && lastRound == 1440 ) ){
            lastRound = 1;
        }else if ( (game.equals("Mario2m") && lastRound == 720 ) || (game.equals("Bubble2m") && lastRound == 720 ) || (game.equals("Bubble2m") && lastRound == 720 ) ){
            lastRound = 1;
        }else if ( (game.equals("Bubble3m") && lastRound == 480 ) || (game.equals("Mario3m") && lastRound == 480 ) || (game.equals("Star3m") && lastRound == 480 ) ){
            lastRound = 1;
        }else {
            lastRound = lastRound + 1;
        }

        cv.setRound(lastRound);

        EgovMap eMap = collectorService.getCurrentPercent(cv);
        BigDecimal oddf = (BigDecimal) eMap.get("odd");
        BigDecimal evenf = (BigDecimal) eMap.get("even");

        remark = remark.concat("홀 : " + oddf + "% 짝:" + evenf + "% ");

        if ( Integer.parseInt(oddf + "") < 46 ){
            odd++;
        }

        if ( Integer.parseInt(evenf + "") < 46 ){
            even++;
        }


        List<CollectorVO> collectorVOList = collectorService.retrieveLastResultOfLimit(cv);
        boolean odd1 = false, odd2 = false, odd3 = false, odd4 = false, odd5 = false, odd6 = false, odd7 = false, odd8 = false, odd9 = false, odd10 = false;

        for( int i = 0 ; i < collectorVOList.size(); i++ ){
             CollectorVO vo = collectorVOList.get(i);

             if( i == 0 ) odd10 = vo.getOdd();
             if( i == 1 ) odd9 = vo.getOdd();
             if( i == 2 ) odd8 = vo.getOdd();
             if( i == 3 ) odd7 = vo.getOdd();
             if( i == 4 ) odd6 = vo.getOdd();
             if( i == 5 ) odd5 = vo.getOdd();
             if( i == 6 ) odd4 = vo.getOdd();
             if( i == 7 ) odd3 = vo.getOdd();
             if( i == 8 ) odd2 = vo.getOdd();
             if( i == 9 ) odd1 = vo.getOdd();
        }

        /*
            50.05 , // 1 꺽
            25.05 , // 2
            18.05 , // 3
            15.00 , // 4
            5.83 , // 5
            2.67 , // 6 줄 구간
            2.33 , // 7
            2.00 , // 8
            0.75 , // 9
            0.10   // 10
         */
        String rate = String.format("%.2f", getRate() * 100.0f);


        // 1. 퐁당 구간

        boolean isPong = false;
        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 4퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 4퐁당 : 홀 ");
            }
        }

        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 5퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 5퐁당 : 홀 ");
            }
        }

        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && odd6 != odd5 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 6퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 6퐁당 : 홀 ");
            }
        }

        // 7 퐁당
        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && odd6 != odd5 && odd5 != odd4 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 7퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 7퐁당 : 홀 ");
            }
        }

        // 8 퐁당
        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && odd6 != odd5 && odd5 != odd4 && odd4 != odd3 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 8퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 8퐁당 : 홀 ");
            }
        }

        // 9 퐁당
        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && odd6 != odd5 && odd5 != odd4 && odd4 != odd3 && odd3 != odd2 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 9퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 9퐁당 : 홀 ");
            }
        }

        // 10 퐁당
        if( odd10 != odd9 && odd9 != odd8 && odd8 != odd7 && odd7 != odd6 && odd6 != odd5 && odd5 != odd4 && odd4 != odd3 && odd3 != odd2 && odd2 != odd1 && rate.equals(reverse) == false ){

            isPong = true;

            if(odd10) {
                even++;
                remark = remark.concat(", 10퐁당 : 짝 ");
            }
            else{
                odd++;
                remark = remark.concat(", 10퐁당 : 홀 ");
            }
        }


        // 2. 줄 구간
        boolean isZang = false;
        if( odd10 == odd9 && rate.equals(reverse) == false ){

            isZang = true;
            if(odd10){
                odd++;
                remark = remark.concat(", 3줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 3줄 : 짝 ");
            }

        }

        // 3줄
        if( odd10 == odd9 && odd9 == odd8 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 3줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 3줄 : 짝 ");
            }

        }

        // 4줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 4줄 : 홀 ");
            }
            else {
                even++;
                remark = remark.concat(", 4줄 : 짝 ");
            }
        }

        // 5줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && rate.equals(reverse) == false ){

            isZang = true;
            if(odd10){
                odd++;
                remark = remark.concat(", 5줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 5줄 : 짝 ");
            }

        }

        // 6줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && odd6 == odd5 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 6줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 6줄 : 짝 ");
            }
        }

        // 7줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && odd6 == odd5 && odd5 == odd4 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 7줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 7줄 : 짝 ");
            }
        }

        // 8줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && odd6 == odd5 && odd5 == odd4 && odd4 == odd3 && rate.equals(reverse) == false ){

            isZang = true;

            if ( rate.equals("2.00") ){

                if(odd10){
                    odd++;
                    remark = remark.concat(", 8줄 : 홀 ");
                }
                else{
                    even++;
                    remark = remark.concat(", 8줄 : 짝 ");
                }

            }
        }

        // 9줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && odd6 == odd5 && odd5 == odd4 && odd4 == odd3 && odd3 == odd2 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 9줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 9줄 : 짝 ");
            }

        }

        // 10줄
        if( odd10 == odd9 && odd9 == odd8 && odd8 == odd7 && odd7 == odd6 && odd6 == odd5 && odd5 == odd4 && odd4 == odd3 && odd3 == odd2 && odd2 == odd1 && rate.equals(reverse) == false ){

            isZang = true;

            if(odd10){
                odd++;
                remark = remark.concat(", 10줄 : 홀 ");
            }
            else{
                even++;
                remark = remark.concat(", 10줄 : 짝 ");
            }

        }

        if( odd == even ){

            if ( !rate.equals(reverse) ){
                if(odd10) odd++;
                else even++;
            }else{
                if(!odd10) odd++;
                else even++;
            }
        }

        remark = remark.concat(", rate : " + rate + "%, 홀 : " + odd + " 짝 : " + even);

        CollectorVO collectorTipVO = new CollectorVO();
        collectorTipVO.setName(cv.getName());
        collectorTipVO.setRound(lastRound);
        collectorTipVO.setLine(l3 > l4);
        collectorTipVO.setStart(left > right);
        collectorTipVO.setOdd(odd > even);
        collectorTipVO.setRemark(remark);

        collectorService.createGameTip(collectorTipVO);
    }

    public double getRate(){

        double[] rate = {
                0.2999 , // 1 꺽
                0.2505 , // 2
                0.1805 , // 3
                0.15   , // 4
                0.0583 , // 5
                0.0267 , // 6 줄 구간
                0.0233 , // 7
                0.02   , // 8
                0.0075 , // 9
                0.001    // 10
        };

        double r = ThreadLocalRandom.current().nextDouble(0, 1);

        if (r <= rate[9]) {
            return rate[9];
        }else if (r <= rate[8]) {
            return rate[8];
        }else if (r <= rate[7]) {
            return rate[7];
        }else if (r <= rate[6]) {
            return rate[6];
        }else if (r <= rate[5]) {
            return rate[5];
        }else if (r <= rate[4]) {
            return rate[4];
        }else if (r <= rate[3]) {
            return rate[3];
        }else if (r <= rate[2]) {
            return rate[2];
        }else if (r <= rate[1]) {
            return rate[1];
        }else if (r <= rate[0]) {
            return rate[0];
        }else{
            return getRate();
        }

    }

    public int addPointLeft(String p, int left, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){
            case "좌3짝":
            case "좌4홀":
                result = left + point;
                break;
        }
        return result;
    }

    public int addPointRight(String p, int right, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){

            case "우3홀":
            case "우4짝":
                result = right + point;
                break;
        }
        return result;
    }

    public int addPointL3(String p, int l3, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){

            case "우3홀":

            case "좌3짝":
                result = l3 + point;
                break;
        }

        return result;
    }

    public int addPointL4(String p, int l4, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){

            case "우4짝":
            case "좌4홀":
                result = l4 + point;
                break;
        }

        return result;
    }

    public int addPointOdd(String p, int odd, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){

            case "우3홀":
            case "좌4홀":
                result = odd + point;
                break;
        }

        return result;
    }

    public int addPointEven(String p, int even, int point){
        int result = 0;

        if (StringUtils.isEmpty(p)) return 0;

        switch(p){

            case "우4짝":
            case "좌3짝":
                result = even + point;
                break;
        }

        return result;
    }

    public void makePattern(String game) throws Exception {
        collectorService.createPattern(game);
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

}
