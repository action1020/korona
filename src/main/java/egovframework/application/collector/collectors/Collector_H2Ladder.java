package egovframework.application.collector.collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class Collector_H2Ladder {

    private static Collector_H2Ladder collector = null;
    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public final static String JSOUP_REF = "http://hgame.tv/Game/h2dari/live.aspx";
    private static final String URL = "http://hgame.tv/game/stats/api.aspx?offset=15&size=15&cmd=get_round_data&type=h2_dari&get_type=round&get_value=288";

    private Collector_H2Ladder() {
    }

    public synchronized static Collector_H2Ladder getInstance() {
        if (null == collector) {
            collector = new Collector_H2Ladder();
        }
        return collector;
    }

    public List<CollectorVO> start() throws Exception {

        List<CollectorVO> collectorVOList = new ArrayList<CollectorVO>();

        List<HashMap<String, String>> URList = new ArrayList<HashMap<String, String>>();
        Connection.Response res = Jsoup.connect(URL).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false).execute();
        Document document = res.parse();
        String response = document.text();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        HashMap<String, Object> hashMap = mapper.readValue(response, HashMap.class);
        List list = (ArrayList) hashMap.get("data");

        for (int i = 0; i < list.size(); i++) {

            CollectorVO collectorVO = new CollectorVO();

            Map m = (Map) list.get(i);

            String idx = (String) m.get("idx");
            String reg_date = (String) m.get("reg_date");
            String round = (String) m.get("round");
            String start_point = (String) m.get("start_point");
            String odd_even = (String) m.get("odd_even");
            String line_count = (String) m.get("line_count");
            String start_line_alias = (String) m.get("start_line_alias");

            collectorVO.setRegdt(reg_date);
            collectorVO.setName("H2Ladder");
            collectorVO.setRound( Integer.parseInt(round));
            collectorVO.setStart( start_point.equals("LEFT") ? true : false); // LEFT tue | RIGHT false
            collectorVO.setOdd( odd_even.equals("ODD") ? true : false); // ODD true | EVEN false
            collectorVO.setLine( line_count.equals("3") ? true : false ); // 3 true | 4 false
            collectorVOList.add(collectorVO);

        }

        return collectorVOList;

    }
}

/*

{idx=124875, reg_date=2020-01-29, round=315, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=37800, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124874, reg_date=2020-01-29, round=314, start_point=RIGHT, start_point_alias=우, odd_even=ODD, odd_even_alias=홀, line_count=3, result_seconds=37680, state=COMPLETE, start_line=RIGHT3ODD, start_line_alias=우3홀}
{idx=124873, reg_date=2020-01-29, round=313, start_point=RIGHT, start_point_alias=우, odd_even=ODD, odd_even_alias=홀, line_count=3, result_seconds=37560, state=COMPLETE, start_line=RIGHT3ODD, start_line_alias=우3홀}
{idx=124872, reg_date=2020-01-29, round=312, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=37440, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124871, reg_date=2020-01-29, round=311, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=37320, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124870, reg_date=2020-01-29, round=310, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=37200, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124869, reg_date=2020-01-29, round=309, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=37080, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124868, reg_date=2020-01-29, round=308, start_point=RIGHT, start_point_alias=우, odd_even=ODD, odd_even_alias=홀, line_count=3, result_seconds=36960, state=COMPLETE, start_line=RIGHT3ODD, start_line_alias=우3홀}
{idx=124867, reg_date=2020-01-29, round=307, start_point=RIGHT, start_point_alias=우, odd_even=ODD, odd_even_alias=홀, line_count=3, result_seconds=36840, state=COMPLETE, start_line=RIGHT3ODD, start_line_alias=우3홀}
{idx=124866, reg_date=2020-01-29, round=306, start_point=RIGHT, start_point_alias=우, odd_even=EVEN, odd_even_alias=짝, line_count=4, result_seconds=36720, state=COMPLETE, start_line=RIGHT4EVEN, start_line_alias=우4짝}
{idx=124865, reg_date=2020-01-29, round=305, start_point=RIGHT, start_point_alias=우, odd_even=ODD, odd_even_alias=홀, line_count=3, result_seconds=36600, state=COMPLETE, start_line=RIGHT3ODD, start_line_alias=우3홀}
{idx=124864, reg_date=2020-01-29, round=304, start_point=LEFT, start_point_alias=좌, odd_even=ODD, odd_even_alias=홀, line_count=4, result_seconds=36480, state=COMPLETE, start_line=LEFT4ODD, start_line_alias=좌4홀}
{idx=124863, reg_date=2020-01-29, round=303, start_point=LEFT, start_point_alias=좌, odd_even=EVEN, odd_even_alias=짝, line_count=3, result_seconds=36360, state=COMPLETE, start_line=LEFT3EVEN, start_line_alias=좌3짝}
{idx=124862, reg_date=2020-01-29, round=302, start_point=LEFT, start_point_alias=좌, odd_even=EVEN, odd_even_alias=짝, line_count=3, result_seconds=36240, state=COMPLETE, start_line=LEFT3EVEN, start_line_alias=좌3짝}
{idx=124861, reg_date=2020-01-29, round=301, start_point=LEFT, start_point_alias=좌, odd_even=EVEN, odd_even_alias=짝, line_count=3, result_seconds=36120, state=COMPLETE, start_line=LEFT3EVEN, start_line_alias=좌3짝}

*/