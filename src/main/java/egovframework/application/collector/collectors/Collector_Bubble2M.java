package egovframework.application.collector.collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class Collector_Bubble2M {

    private static Collector_Bubble2M collector = null;
    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public final static String JSOUP_REF = "http://mini-1010.com/bobble_2min.php";
    private static final String URL = "http://mini-1010.com/api_bobble2min.php";

    private Collector_Bubble2M() {
    }

    public synchronized static Collector_Bubble2M getInstance() {
        if (null == collector) {
            collector = new Collector_Bubble2M();
        }
        return collector;
    }

    public List<CollectorVO> start() throws Exception {

        List<CollectorVO> collectorVOList = new ArrayList<CollectorVO>();

        List<HashMap<String, String>> URList = new ArrayList<HashMap<String, String>>();
        Connection.Response res = Jsoup.connect(URL + "?_=" + new Date().getTime()).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false)
                .method(Connection.Method.GET)
                .execute();
        Document document = res.parse();
        String response = document.text();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        HashMap<String, Object> m = mapper.readValue(response, HashMap.class);

        CollectorVO collectorVO = new CollectorVO();

        String reg_date = (String) m.get("d");
        String round = (String) m.get("r");
        String start_point = (String) m.get("s");
        String odd_even = (String) m.get("o");
        String line_count = (String) m.get("l");

        collectorVO.setRegdt(reg_date);
        collectorVO.setName("Bubble2m");
        collectorVO.setRound( Integer.parseInt(round));
        collectorVO.setStart( start_point.equals("LEFT") ? true : false); // LEFT tue | RIGHT false
        collectorVO.setOdd( odd_even.equals("ODD") ? true : false); // ODD true | EVEN false
        collectorVO.setLine( line_count.equals("3") ? true : false ); // 3 true | 4 false
        collectorVOList.add(collectorVO);



        return collectorVOList;

    }
}

/*
    {"d":"2020-02-18","r":"1197","o":"EVEN","s":"LEFT","l":"3","rateOdd":"30","rateEven":"70"}
*/