package egovframework.application.collector.collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class Collector_Bubble2_1M {

    private static Collector_Bubble2_1M collector = null;
    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public final static String JSOUP_REF = "http://st-ppl.com/game/ladder1m/";
    private static final String URL1 = "http://st-ppl.com/game/ladder1m/ajax.get_live_data.php";
    private static final String URL2 = "http://st-ppl.com/game/ladder1m/ajax.try_result.php";

    private Collector_Bubble2_1M() {
    }

    public synchronized static Collector_Bubble2_1M getInstance() {
        if (null == collector) {
            collector = new Collector_Bubble2_1M();
        }
        return collector;
    }

    public List<CollectorVO> start() throws Exception {

        List<CollectorVO> collectorVOList = new ArrayList<CollectorVO>();
        List<HashMap<String, String>> URList = new ArrayList<HashMap<String, String>>();
        Document document = null;

        do {
            Connection.Response res = Jsoup.connect(URL1).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false)
                    .method(Connection.Method.GET)
                    .execute();
            document = res.parse();

            String response = document.text();

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            HashMap<String, Object> r1 = mapper.readValue(response, HashMap.class);

            int timer = (Integer) r1.get("remainTime");

            if (timer < 0)
                break;

        } while(true);


        for ( int i = 0; i < 5; i++ ){

            Connection.Response res = Jsoup.connect(URL2).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false)
                    .method(Connection.Method.GET)
                    .execute();

            document = res.parse();

            if( document.text().equals("{\"result\":\"try-again\"}")) {
                Thread.sleep(1000);
                continue;
            }else if ( document.text().equals("{\"result\":\"stopped\"}")) {
                Thread.sleep(1000);
                continue;
            }else if ( document.text().equals("{\"result\":\"reload\"}")) {
                Thread.sleep(1000);
                continue;
            }else{
                break;
            }
        }

        String response = document.text();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        HashMap<String, Object> r = mapper.readValue(response, HashMap.class);
        HashMap<String, Object> m = (HashMap) r.get("result");

        CollectorVO collectorVO = new CollectorVO();

        String reg_date = (String) m.get("game_datetime");
        String round = (String) m.get("game_no");
        String start_point = (String) m.get("game_result_lr");
        String odd_even = (String) m.get("game_result_oe");
        String line_count = (String) m.get("game_result_34");

        collectorVO.setRegdt(reg_date.substring(0,10));
        collectorVO.setName("Bubble2_1m");
        collectorVO.setRound( Integer.parseInt(round));
        collectorVO.setStart( start_point.equals("1") ? true : false); // LEFT tue | RIGHT false
        collectorVO.setOdd( odd_even.equals("1") ? true : false); // ODD true | EVEN false
        collectorVO.setLine( line_count.equals("3") ? true : false ); // 3 true | 4 false
        collectorVOList.add(collectorVO);

        return collectorVOList;

    }
}

/*
   :{"game_no":"1170","game_datetime":"2020-02-28 19:29:00","game_status":"\uc644\ub8cc","game_result_oe":"2","game_result_lr":"2","game_result_34":"4"}}
*/