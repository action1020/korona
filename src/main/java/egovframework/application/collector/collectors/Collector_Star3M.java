package egovframework.application.collector.collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class Collector_Star3M {

    private static Collector_Star3M collector = null;
    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public final static String JSOUP_REF = "http://boscore.com/ladder/live1.php";
    private static final String URL = "http://boscore.com/plugin/ladder/ladder_result.php";

    private Collector_Star3M() {
    }

    public synchronized static Collector_Star3M getInstance() {
        if (null == collector) {
            collector = new Collector_Star3M();
        }
        return collector;
    }

    public List<CollectorVO> start() throws Exception {

        List<CollectorVO> collectorVOList = new ArrayList<CollectorVO>();

        List<HashMap<String, String>> URList = new ArrayList<HashMap<String, String>>();
        Connection.Response res = Jsoup.connect(URL).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false)
                .data("ld_mode", "3")
                .method(Connection.Method.POST)
                .execute();

        Document document = res.parse();
        String response = document.text();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        HashMap<String, Object> m = mapper.readValue(response, HashMap.class);

        CollectorVO collectorVO = new CollectorVO();

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        String reg_date = sdf.format(System.currentTimeMillis());
        Integer round = (Integer) m.get("times");
        String start_point = (String) m.get("start_point");
        String odd_even = (String) m.get("answer");
        String line_count = (String) m.get("ladder_type");

        collectorVO.setRegdt(reg_date);
        collectorVO.setName("Star3m");
        collectorVO.setRound( round );
        collectorVO.setStart( start_point.equals("first") ? true : false); // LEFT tue | RIGHT false
        collectorVO.setOdd( odd_even.equals("odd") ? true : false); // ODD true | EVEN false
        collectorVO.setLine( line_count.equals("type1") ? true : false ); // 3 true | 4 false
        collectorVOList.add(collectorVO);

        return collectorVOList;

    }
}

/*
    {"times":51,"times2":"02\uc6d419\uc77c-51","start_point":"first","ladder_type":"type1","answer":"even","answer2":"\uc9dd","msg":" <\/li> <\/li> <\/li> <\/li> <\/li> <\/li> <\/li>"}
*/