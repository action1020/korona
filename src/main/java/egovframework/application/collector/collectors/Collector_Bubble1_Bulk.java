package egovframework.application.collector.collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.application.domain.CollectorVO;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class Collector_Bubble1_Bulk {

    private static Collector_Bubble1_Bulk collector = null;
    public final static String JSOUP_AGENT = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
    private final Integer JSOUP_TIMEOUT = 30 * 1000; // ( 30 Second )
    private final Integer JSOUP_FILE_TIMEOUT = (60 * 1000); // (60 Second )
    public final static String JSOUP_REF = "https://bepick.net/game/popup_pattern/bubble_ladder1/";
    private static final String URL = "https://bepick.net/api/get_pattern/bubble_ladder1/daily/fd4/1/";

    private Collector_Bubble1_Bulk() {
    }

    public synchronized static Collector_Bubble1_Bulk getInstance() {
        if (null == collector) {
            collector = new Collector_Bubble1_Bulk();
        }
        return collector;
    }

    public List<CollectorVO> start(String date) throws Exception {

        List<CollectorVO> collectorVOList = new ArrayList<CollectorVO>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

        String reg_date = sdf2.format(sdf.parse(date));

        List<HashMap<String, String>> URList = new ArrayList<HashMap<String, String>>();
        Connection.Response res = Jsoup.connect(URL + date + "?_=" + new Date().getTime()).ignoreContentType(true).userAgent(JSOUP_AGENT).referrer(JSOUP_REF + date ).timeout(JSOUP_TIMEOUT).maxBodySize(0).followRedirects(true).validateTLSCertificates(false)
                .method(Connection.Method.GET)
                .execute();
        Document document = res.parse();
        String response = document.text();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        HashMap<String, Object> hashMap = mapper.readValue(response, HashMap.class);
        List list = (ArrayList) hashMap.get("list");
        for( int i = 0; i < list.size(); i++ ) {

            Map m = (Map) list.get(i);
            CollectorVO collectorVO = new CollectorVO();


            Integer round = (Integer) m.get("round");
            Integer result = (Integer) m.get("result");

            String start_point = "";
            String odd_even = "";
            String line_count = "";

            switch( result ) {

                case 1 :// 1 좌3짝
                    start_point = "LEFT";
                    odd_even = "EVEN";
                    line_count = "3";
                    break;
                case 2 :// 2 좌4홀
                    start_point = "LEFT";
                    odd_even = "ODD";
                    line_count = "4";
                    break;
                case 3 :// 3 우3홀
                    start_point = "RIGHT";
                    odd_even = "ODD";
                    line_count = "3";
                    break;
                case 4:// 4 우4짝
                    start_point = "RIGHT";
                    odd_even = "EVEN";
                    line_count = "4";
                    break;

            }

            collectorVO.setRegdt(reg_date);
            collectorVO.setName("Bubble2_1m");
            collectorVO.setRound( round );
            collectorVO.setStart( start_point.equals("LEFT") ? true : false); // LEFT tue | RIGHT false
            collectorVO.setOdd( odd_even.equals("ODD") ? true : false); // ODD true | EVEN false
            collectorVO.setLine( line_count.equals("3") ? true : false ); // 3 true | 4 false
            collectorVOList.add(collectorVO);

        }




        return collectorVOList;

    }
}

/*
    {"d":"2020-02-18","r":"1197","o":"EVEN","s":"LEFT","l":"3","rateOdd":"30","rateEven":"70"}
*/