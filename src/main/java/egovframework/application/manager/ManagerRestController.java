package egovframework.application.manager;

import egovframework.application.domain.SearchVO;
import egovframework.application.domain.UserVO;
import egovframework.application.manager.service.ManagerService;
import egovframework.application.manager.support.SessionManager;
import egovframework.framework.contants.Ajax;
import egovframework.rte.fdl.cmmn.exception.EgovBizException;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description ManagerRestController.java
 */
@Slf4j
@RestController
@RequestMapping(value = "/manager/rest/", produces = "application/json")
public class ManagerRestController {

    @Resource(name = "managerService")
    private ManagerService managerService;

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    @Resource(name = "authenticationManager")
    private AuthenticationManager authenticationManager;

    /**
     * loginCheck
     *
     * @param request
     * @param userVO
     * @return
     */
    @RequestMapping(value = "loginCheck", method = RequestMethod.POST)
    public EgovMap loginCheck(HttpServletRequest request, UserVO userVO) {
        log.debug("loginCheck");

        EgovMap res = new EgovMap();

        try {

            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(userVO.getLoginId(), userVO.getPassword());
            Authentication authentication = authenticationManager.authenticate(authRequest);
            SecurityContext securityContext = new SecurityContextImpl();
            securityContext.setAuthentication(authentication);
            SecurityContextHolder.setContext(securityContext);
            request.getSession().setAttribute("SYSTEM_SPRING_SECURITY_CONTEXT", securityContext);

            //userVO = managerService.findByUserById(userVO);

            sessionManager.createSession(request, userVO);
            res.put("result", Ajax.OK.result());

        } catch (BadCredentialsException e) {

            res.put("message", "아이디 또는 비밀번호가 일치하지 않습니다.");
            res.put("result", Ajax.NO.result());

        } catch (Exception e) {

            e.printStackTrace();
            log.error(e.getMessage(), e.fillInStackTrace());
            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    /**
     * retrieveList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "retrieveList")
    public EgovMap retrieveList(SearchVO searchVO) {

        log.debug("retrieveList");
        EgovMap res = new EgovMap();

        try {

            List<EgovMap> list = managerService.retrieveList(searchVO);

             res.put("page", searchVO.getPage(list));
            res.put("list", list);
            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    /**
     * passwordUpdate
     *
     * @param request
     * @param userVO
     * @return
     */
    @RequestMapping(value = "passwordUpdate", method = RequestMethod.POST)
    public EgovMap passwordUpdate(HttpServletRequest request, UserVO userVO) {
        log.debug("passwordUpdate");

        EgovMap res = new EgovMap();

        try {

            managerService.passwordUpdate(request, userVO);

            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.getMessage(), e.fillInStackTrace());
            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    /**
     * createOrUpdate
     *
     * @param request
     * @param userVO
     * @return
     */
    @RequestMapping(value = "createOrUpdate", method = RequestMethod.POST)
    public EgovMap createOrUpdate(UserVO userVO) {
        log.debug("createOrUpdate");

        EgovMap res = new EgovMap();

        try {

            managerService.createOrUpdate(userVO);

            res.put("result", Ajax.OK.result());

        } catch (EgovBizException e) {

            log.error(e.getMessage(), e.fillInStackTrace());
            res.put("message", e.getMessage());
            res.put("result", Ajax.NO.result());

        } catch (Exception e) {

            log.error(e.getMessage(), e.fillInStackTrace());
            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;
    }

    /**
     * removeRows
     *
     * @param userVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "removeRows")
    public EgovMap removeRows(UserVO userVO) {

        log.debug("removeRows");
        EgovMap res = new EgovMap();

        try {

            managerService.removeRows(userVO);

            res.put("result", Ajax.OK.result());

        } catch (Exception e) {

            log.error(e.toString(), e.fillInStackTrace());

            res.put("message", e.toString());
            res.put("result", Ajax.NO.result());

        }

        return res;

    }

}