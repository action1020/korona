package egovframework.application.manager.support;

import egovframework.application.domain.UserVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description SessionManager.java
 */
@Component(value = "sessionManager")
public class SessionManager {

    @Resource(name = "propertiesService")
    private EgovPropertyService props;

    /**
     * isLogin
     *
     * @param request
     * @return
     */
    public boolean isLogin(HttpServletRequest request) {

        String sessionId = props.getString("WEB_SESSION");
        return request.getSession().getAttribute(sessionId) != null;

    }

    /**
     * createSession
     *
     * @param request
     * @param userVO
     */
    public void createSession(HttpServletRequest request,
                              UserVO userVO) {

        String sessionId = props.getString("WEB_SESSION");
        request.getSession().setAttribute(sessionId, userVO);


    }

    /**
     * getSession
     *
     * @param request
     * @return
     */
    public UserVO getSession(HttpServletRequest request) {

        String sessionId = props.getString("WEB_SESSION");
        UserVO userVO = (UserVO) request.getSession().getAttribute(sessionId);

        return userVO;

    }

    /**
     * getUserId
     *
     * @param request
     * @return
     */
    public String getUserId(HttpServletRequest request) {

        String sessionId = props.getString("WEB_SESSION");
        UserVO userVO = (UserVO) request.getSession().getAttribute(sessionId);
        return userVO.getLoginId();

    }

}