package egovframework.application.manager;

import egovframework.application.manager.support.SessionManager;
import egovframework.rte.fdl.property.EgovPropertyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description ManagerController.java
 */
@Slf4j
@Controller
public class ManagerController {

    @Resource(name = "propertiesService")
    private EgovPropertyService props;

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    /**
     * loginPage
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"", "/login"})
    public String loginPage(HttpServletRequest request) {
        log.debug("loginPage");

        if (sessionManager.isLogin(request)) {
            return "redirect:/collector/collectorList";
        } else {
            return "/manager/login";
        }
    }

    /**
     * logout
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        log.debug("logout");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return "redirect:/login";

    }

    /**
     * retrieveUserList
     *
     * @return
     */
    @RequestMapping(value = "/manager/retrieveUserList")
    public String retrieveUserList() {
        log.debug("retrieveUserList");
        return "/manager/userList";
    }

}