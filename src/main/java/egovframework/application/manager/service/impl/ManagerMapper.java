package egovframework.application.manager.service.impl;

import egovframework.application.domain.SearchVO;
import egovframework.application.domain.UserVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description ManagerServiceImpl.java
 */
@Mapper(value = "managerMapper")
public interface ManagerMapper {

    /**
     * retrieveUserInfo
     *
     * @param userVO
     * @throws Exception
     */
    UserVO retrieveUserInfo(UserVO userVO) throws Exception;

    /**
     * retrieveList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retrieveList(SearchVO searchVO) throws Exception;

    /**
     * passwordUpdate
     *
     * @param userVOfromSession
     * @throws Exception
     */
    void passwordUpdate(UserVO userVOfromSession) throws Exception;

    /**
     * create
     *
     * @param userVO
     * @throws Exception
     */
    void create(UserVO userVO) throws Exception;

    /**
     * update
     *
     * @param userVO
     * @throws Exception
     */
    void update(UserVO userVO) throws Exception;

    /**
     * removeRows
     *
     * @param userVO
     * @throws Exception
     */
    void removeRows(UserVO userVO) throws Exception;
}