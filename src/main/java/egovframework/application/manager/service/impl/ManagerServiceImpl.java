package egovframework.application.manager.service.impl;

import egovframework.application.domain.SearchVO;
import egovframework.application.domain.UserVO;
import egovframework.application.manager.service.ManagerService;
import egovframework.application.manager.support.SessionManager;
import egovframework.framework.support.TApassword;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.cmmn.exception.EgovBizException;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description ManagerServiceImpl.java
 */
@Slf4j
@Service(value = "managerService")
public class ManagerServiceImpl extends EgovAbstractServiceImpl implements ManagerService {

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    @Resource(name = "managerMapper")
    private ManagerMapper managerMapper;

    /**
     * findByUserById
     *
     * @param userVO
     * @return
     * @throws Exception
     */
    @Override
    public UserVO findByUserById(UserVO userVO) throws Exception {
        log.debug("findByUserById");
        return managerMapper.retrieveUserInfo(userVO);
    }

    /**
     * retrieveList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    @Override
    public List<EgovMap> retrieveList(SearchVO searchVO) throws Exception {
        log.debug("retrieveList");
        return managerMapper.retrieveList(searchVO);
    }

    /**
     * passwordUpdate
     *
     * @param request
     * @param userVO
     * @throws Exception
     */
    @Override
    public void passwordUpdate(HttpServletRequest request, UserVO userVO) throws Exception {
        log.debug("passwordUpdate");

        UserVO userVOfromSession = sessionManager.getSession(request);
        userVOfromSession.setPassword(TApassword.getSHA256(userVO.getPassword()));
        managerMapper.passwordUpdate(userVOfromSession);
    }

    /**
     * createOrUpdate
     *
     * @param userVO
     * @throws Exception
     */
    @Override
    public void createOrUpdate(UserVO userVO) throws Exception {
        log.debug("createOrUpdate");

        if (StringUtils.isNotEmpty(userVO.getPassword())) {
            userVO.setPassword(TApassword.getSHA256(userVO.getPassword()));
        }

        if (userVO.getId() == null || userVO.getId() < 0) {

            UserVO userVOfromDB = managerMapper.retrieveUserInfo(userVO);

            if (null != userVOfromDB && (userVOfromDB.getSeq() != null && userVOfromDB.getSeq() > 0)) {
                throw new EgovBizException("이미 사용중인 아이디입니다.");
            }

            managerMapper.create(userVO);

        } else {
            managerMapper.update(userVO);
        }

    }

    /**
     * removeRows
     *
     * @param userVO
     * @throws Exception
     */
    @Override
    public void removeRows(UserVO userVO) throws Exception {
        log.debug("removeRows");
        managerMapper.removeRows(userVO);
    }
}