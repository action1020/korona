package egovframework.application.manager.service;

import egovframework.application.domain.SearchVO;
import egovframework.application.domain.UserVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author Yi.yh (action1020s@ gmail.com)
 * @date 2018. 4. 3.
 * @description ManagerService.java
 */
public interface ManagerService {

    /**
     * findByUserById
     *
     * @param userVO
     * @throws Exception
     */
    UserVO findByUserById(UserVO userVO) throws Exception;

    /**
     * retrieveList
     *
     * @param searchVO
     * @return
     * @throws Exception
     */
    List<EgovMap> retrieveList(SearchVO searchVO) throws Exception;

    /**
     * passwordUpdate
     *
     * @param userVO
     * @throws Exception
     */
    void passwordUpdate(HttpServletRequest request, UserVO userVO) throws Exception;

    /**
     * createOrUpdate
     *
     * @param userVO
     * @throws Exception
     */
    void createOrUpdate(UserVO userVO) throws Exception;

    /**
     * removeRows
     *
     * @param userVO
     * @throws Exception
     */
    void removeRows(UserVO userVO) throws Exception;
}