package egovframework.framework.viewer;

import org.springframework.web.servlet.ModelAndView;


/*
 * by action1020
 */
public interface Viewer {

    int MAX_ROWS = 66535;

    void init(Object object);

    void push(String field, String head);

    boolean hasField(Class<?> clazz, String field, int loop);

    void setSheetName(String sheetName);

    void setFileName(String fileName);

    ModelAndView download(Object object);

    void setViewerConvert(ViewerConvert viewerConvert);

}
