package egovframework.framework.viewer;

import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * by action1020
 */
public class CsvUtil extends CsvView implements Viewer {

    private Queue<String> fieldQueue = new LinkedList<String>();
    private Queue<String> headQueue = new LinkedList<String>();
    private String[] fieldArray = null;
    private String fileName = "CsvFile";
    private ViewerConvert viewerConvert;
    private StringBuffer sb = new StringBuffer();

    @Override
    public void init(Object object) {
        // TODO Auto-generated method stub

        fieldArray = new String[fieldQueue.size()];
        int count = 0;

        while (fieldQueue.peek() != null) {

            fieldArray[count] = fieldQueue.poll();
            count++;

        }

        count = 0;
        while (headQueue.peek() != null) {

            count++;
            sb.append("\"" + headQueue.poll() + "\"");

            if (headQueue.size() > 0) {
                sb.append(",");
            }

        }

        try {

            if (object instanceof List) {

                List<?> listObject = (List<?>) object;

                if (listObject != null && listObject.size() > 0) {

                    for (Object firstObject : listObject) {

                        if (firstObject instanceof Map) {

                            Map<?, ?> map = (Map<?, ?>) firstObject;

                            sb.append("\n");

                            for (int i = 0; i < fieldArray.length; i++) {

                                Object obj = map.get(fieldArray[i].toLowerCase());

                                if (obj != null) {

                                    String s = obj.toString() == null ? "" : obj.toString();

                                    if (hasConvert()) {

                                        try {

                                            s = (String) viewerConvert.convert(fieldArray[i], obj);

                                        } catch (Exception e) {

                                            s = "";
                                        }
                                    }

                                    sb.append("\"" + s + "\"");

                                    if (i < fieldArray.length) {

                                        sb.append(",");

                                    }
                                }

                            }

                        } else {

                            Class<? extends Object> clazz = firstObject.getClass();

                            sb.append("\n");

                            int loop = count;

                            for (int i = 0; i < fieldArray.length; i++) {

                                if (hasField(clazz, fieldArray[i], i)) {

                                    String fieldName = fieldArray[i];

                                    String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

                                    Method getMethod = clazz.getMethod(getMethodName);
                                    Object obj = getMethod.invoke(firstObject);
                                    String s = obj.toString() == null ? "" : obj.toString();

                                    if (hasConvert()) {

                                        try {

                                            s = (String) viewerConvert.convert(fieldName, obj);

                                        } catch (Exception e) {

                                            s = "";
                                        }
                                    }


                                    sb.append("\"" + s + "\"");

                                    loop--;

                                    if (loop > 0) {

                                        sb.append(",");

                                    }

                                }
                            }

                        }

                    }

                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public void push(String field, String head) {
        // TODO Auto-generated method stub

        fieldQueue.add(field);

        headQueue.add(head);

    }

    @Override
    public boolean hasField(Class clazz, String field, int loop) {
        // TODO Auto-generated method stub

        if (loop > 0) {

            return true;

        }

        try {

            clazz.getDeclaredField(field);

        } catch (Exception e) {

            return false;

        }

        return true;

    }

    /*
     * Not used !
     * @see com.sycros.AppFrameWork.bean.SCWebConsoleViewer#setSheetName(java.lang.String)
     */
    @Override
    public void setSheetName(String sheetName) {
    }

    @Override
    public void setFileName(String fileName) {
        // TODO Auto-generated method stub

        this.fileName = fileName;

    }

    @Override
    public ModelAndView download(Object object) {
        // TODO Auto-generated method stub

        try {

            init(object);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return new ModelAndView(this);

    }

    @Override
    public void setViewerConvert(ViewerConvert viewerConvert) {
        // TODO Auto-generated method stub

        this.viewerConvert = viewerConvert;

    }

    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> model,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

        response.setHeader("Content-Type", "text/csv; charset=MS949");
        response.setHeader("Content-Disposition", "attachment;filename=" + java.net.URLEncoder.encode(this.fileName + "_" + sdf.format(c.getTime()) + ".csv", "UTF-8").replace('+', ' ').replaceAll(" ", "%20"));

        PrintWriter out = response.getWriter();
        out.print(sb.toString());

    }
}
