package egovframework.framework.viewer;

import org.springframework.web.servlet.view.AbstractView;

/*
 * by action1020
 */
public abstract class CsvView extends AbstractView {

    protected ViewerConvert viewerConvert;

    /*
     * SCwebConsoleViewer 인터페이스를 구현한 구현체가 컨버터 클래스를
     * 소유하고있는지 리턴한다.
     */
    protected final boolean hasConvert() {

        return viewerConvert != null;

    }

}
