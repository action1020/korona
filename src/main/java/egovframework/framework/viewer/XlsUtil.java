package egovframework.framework.viewer;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * by action1020
 */
@Controller
public class XlsUtil extends ExcelView implements Viewer {

    private Queue<String> fieldQueue = new LinkedList<String>();
    private Queue<String> headQueue = new LinkedList<String>();
    private String[] fieldArray = null;
    private HSSFWorkbook workbook = new HSSFWorkbook();
    private String sheetName = "Default";
    private String fileName = "Default";

    @SuppressWarnings("deprecation")
    @Override
    public void init(Object object) {
        // TODO Auto-generated method stub

        HSSFSheet sheet = workbook.createSheet();
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        workbook.setSheetName(0, sheetName);
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell;

        fieldArray = new String[fieldQueue.size()];
        int count = 0;

        while (fieldQueue.peek() != null) {

            fieldArray[count] = fieldQueue.poll();
            count++;

        }

        font.setFontName("Arial");
        font.setColor(HSSFFont.COLOR_NORMAL);

        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setFont(font);

        count = 0;

        while (headQueue.peek() != null) {

            String lang = headQueue.poll();
            cell = row.createCell(count);
            cell.setCellStyle(style);
            cell.setCellValue(lang);

            sheet.setColumnWidth(count, (256 * 20));

            count++;
        }

        try {

            if (object instanceof List) {

                List<?> listObject = (List<?>) object;

                if (listObject != null && listObject.size() > 0) {

                    int statRowNum = 1;

                    for (Object dataObject : listObject) {

                        row = sheet.createRow(statRowNum);
                        statRowNum++;

                        int startCellNum = 0;

                        if (dataObject instanceof Map) {


                            Map<?, ?> map = (Map<?, ?>) dataObject;

                            for (int i = 0; i < fieldArray.length; i++) {

                                Object obj = map.get(fieldArray[i].toLowerCase());

                                if (obj != null) {

                                    String s = obj.toString() == null ? "" : obj.toString();

                                    cell = row.createCell(startCellNum++);

                                    /*
                                     * has Convert Class
                                     */
                                    if (hasConvert()) {

                                        try {

                                            s = (String) viewerConvert.convert(fieldArray[i], s);

                                        } catch (Exception e) {

                                            s = "";

                                        }

                                    }

                                    cell.setCellValue(s);

                                } else {

                                    cell = row.createCell(startCellNum++);
                                    cell.setCellValue("");

                                }

                            }


                        } else {

                            Class<? extends Object> clazz = dataObject.getClass();

                            for (int i = 0; i < fieldArray.length; i++) {

                                if (hasField(clazz, fieldArray[i], i)) {

                                    String fieldName = fieldArray[i];
                                    String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

                                    Method getMethod = clazz.getMethod(getMethodName);
                                    Object obj = getMethod.invoke(dataObject);

                                    cell = row.createCell(startCellNum++);
                                    boolean converted = false;

                                    try {

                                        /*
                                         * has Convert Class
                                         */
                                        if (hasConvert()) {

                                            Object beforeConvertObj = obj;

                                            obj = viewerConvert.convert(fieldName, obj);

                                            if (beforeConvertObj.equals(obj)) {

                                                converted = false;

                                            } else {

                                                obj = obj == null ? "" : obj;
                                                cell.setCellValue((String) obj);
                                                converted = true;

                                            }

                                        }

                                        if (!converted) {

                                            obj = obj == null ? "" : obj;

                                            if (getMethod.getReturnType().equals(int.class) || getMethod.getReturnType().equals(Integer.class)) {

                                                cell.setCellValue(Integer.parseInt(obj.toString()));

                                            } else if (getMethod.getReturnType().equals(Double.class) || getMethod.getReturnType().equals(double.class)
                                                    || getMethod.getReturnType().equals(Float.class) || getMethod.getReturnType().equals(float.class)) {

                                                cell.setCellValue(Double.parseDouble(obj.toString()));

                                            } else if (getMethod.getReturnType().equals(Boolean.class) || getMethod.getReturnType().equals(boolean.class)) {

                                                cell.setCellValue(Boolean.parseBoolean(obj.toString()));

                                            } else if (getMethod.getReturnType().equals(Date.class)) {

                                                cell.setCellValue(Date.parse(obj.toString()));

                                            } else {

                                                cell.setCellValue(obj.toString());

                                            }

                                        }

                                    } catch (Exception e) {

                                        cell.setCellValue("");

                                    }

                                }

                            }

                        }

                    }

                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public void push(String field, String head) {
        // TODO Auto-generated method stub

        fieldQueue.add(field);

        headQueue.add(head);

    }

    @Override
    public boolean hasField(Class<? extends Object> clazz, String field, int loop) {
        // TODO Auto-generated method stub

        if (loop > 0) {

            return true;
        }

        try {

            clazz.getDeclaredField(field);

        } catch (Exception e) {

            return false;

        }

        return true;

    }

    @Override
    public void setSheetName(String sheetName) {
        // TODO Auto-generated method stub

        this.sheetName = sheetName;

    }

    @Override
    public void setFileName(String fileName) {
        // TODO Auto-generated method stub

        this.fileName = fileName;

    }

    @Override
    public ModelAndView download(Object object) {
        // TODO Auto-generated method stub

        try {

            init(object);

        } catch (Exception e) {

            //e.printStackTrace();

        }

        return new ModelAndView(this);

    }

    @Override
    public void setViewerConvert(ViewerConvert viewerConvert) {
        // TODO Auto-generated method stub

        this.viewerConvert = viewerConvert;

    }

    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> model,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

        response.setContentType("application/vnd.ms-excel:UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + java.net.URLEncoder.encode(fileName + "_" + sdf.format(c.getTime()) + ".xls", "UTF-8").replace('+', ' ').replaceAll(" ", "%20"));

        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);
        out.flush();

    }
}