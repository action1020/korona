package egovframework.framework.viewer;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

public class DownloadView extends AbstractView {

    private EgovMap result;

    public DownloadView(EgovMap result) {
        this.result = result;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String fileName = getDownFileNames(request, (String) result.get("originfile"));
        String fileSize = String.valueOf(result.get("filesize"));

        response.setContentType("application/download; utf-8");
        response.setContentLength(Integer.parseInt(fileSize));
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);

        OutputStream out = response.getOutputStream();
        FileInputStream fis = null;

        File file = new File((String) result.get("savepath"), (String) result.get("savefile"));

        try {
            fis = new FileInputStream(file);
            FileCopyUtils.copy(fis, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                }
            }
        }
        out.flush();
    }

    public String getDownFileNames(HttpServletRequest request, String realName) {

        String header = request.getHeader("User-Agent");
        String resultName = null;

        try {

            // Explorer
            if (header.indexOf("MSIE") > -1) {
                resultName = URLEncoder.encode(realName, "UTF-8").replaceAll("\\+", "%20");
            }
            // Chrome
            else if (header.indexOf("Chrome") > -1) {
                resultName = new String(realName.getBytes("utf-8"), "8859_1");
            }
            // Opera
            else if (header.indexOf("Opera") > -1) {
                resultName = "\"" + new String(realName.getBytes("UTF-8"), "8859_1") + "\"";
            }
            // Safari
            else if (header.indexOf("Safari") > -1) {
                resultName = new String(realName.getBytes("utf-8"), "8859_1");
            }
            // FireFox
            else if (header.indexOf("Firefox") != -1) {
                resultName = "\"" + new String(realName.getBytes("UTF-8"), "8859_1") + "\"";
            }
            // Other
            else
                resultName = new String(realName.getBytes("euc-kr"), "8859_1");

        } catch (Exception ex) {
            resultName = realName;
        }

        return resultName;
    }

}