package egovframework.framework.viewer;

import egovframework.rte.psl.dataaccess.util.EgovMap;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.Map;

public class DatabaseDownloadView extends AbstractView {

    private EgovMap result;

    public DatabaseDownloadView(EgovMap result) {
        this.result = result;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String fileName = String.valueOf(result.get("filename"));
        String fileSize = String.valueOf(result.get("filesize"));

        response.setContentType("application/download; utf-8");
        response.setContentLength(Integer.parseInt(fileSize));
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);

        byte[] bytes = (byte[]) result.get("bin");

        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        OutputStream os = response.getOutputStream();

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = in.read(buffer)) != -1) {
            os.write(buffer, 0, bytesRead);
        }

        os.close();
        os.flush();
    }

}