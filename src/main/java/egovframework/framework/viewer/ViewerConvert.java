package egovframework.framework.viewer;

/*
 * by action1020
 */
public interface ViewerConvert {

    Object convert(String fieldName, Object value);

}