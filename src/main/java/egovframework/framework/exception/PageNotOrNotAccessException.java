package egovframework.framework.exception;

import org.springframework.core.NestedRuntimeException;

/*************************************************************************************************************************************************************************************
 * 페이지를 찾지 못했거나 권한이 없을때 발생하는 예외처리 클래스
 *
 * @author YoungHo
 * @since 2016. 5. 16.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class PageNotOrNotAccessException extends NestedRuntimeException {

    private static final long serialVersionUID = 1L;

    public PageNotOrNotAccessException(String msg) {
        super(msg);
    }

    public PageNotOrNotAccessException(String msg, Throwable cause) {
        super(msg, cause);
    }

}