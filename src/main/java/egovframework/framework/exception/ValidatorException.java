package egovframework.framework.exception;


/**************************************************************************************************************************************************************************************
 * 벨리데이션 예외처리 익셉션 클래스
 *
 * @author YoungHo
 * @since 2016. 2. 29.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class ValidatorException extends Exception {

    private static final long serialVersionUID = 1L;

    private String errorCode;
    private Object[] arguments;

    public ValidatorException() {
        super();
    }

    public ValidatorException(String message, Throwable cause,
                              boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidatorException(String message) {
        super(message);
    }

    public ValidatorException(Throwable cause) {
        super(cause);
    }

    public ValidatorException(String errorCode, Object[] arguments) {
        super();
        this.errorCode = errorCode;
        this.arguments = arguments;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public Object[] getArguments() {
        return arguments;
    }
}