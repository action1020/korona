package egovframework.framework.exception;

import org.springframework.core.NestedRuntimeException;

/*************************************************************************************************************************************************************************************
 * 파일 업로드 관련 Exception
 *
 * @author YoungHo
 * @since 2016. 4. 28.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class FileUploadException extends NestedRuntimeException {

    private static final long serialVersionUID = 1L;

    public FileUploadException(String msg) {
        super(msg);
    }

    public FileUploadException(String msg, Throwable cause) {
        super(msg, cause);
    }

}