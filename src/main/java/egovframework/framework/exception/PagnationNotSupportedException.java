package egovframework.framework.exception;

import org.springframework.core.NestedRuntimeException;

/*************************************************************************************************************************************************************************************
 * 조회용 VO 클래스가 CommonVO 를 상속하지 않았을때 발생하는 Exception
 *
 * @author YoungHo
 * @since 2016. 2. 25.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class PagnationNotSupportedException extends NestedRuntimeException {

    private static final long serialVersionUID = 1L;

    public PagnationNotSupportedException(String msg) {
        super(msg);
    }

    public PagnationNotSupportedException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
