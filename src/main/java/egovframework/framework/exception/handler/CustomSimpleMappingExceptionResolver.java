package egovframework.framework.exception.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**************************************************************************************************************************************************************************************
 * 커스텀 심플 익셉션 리졸버 클래스 (do/json(AJAX)) 분기 처리
 *
 * @author YoungHo
 * @since 2016. 3. 22.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class CustomSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

    private static final Logger logger = LoggerFactory.getLogger(CustomSimpleMappingExceptionResolver.class);

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response,
                                              Object handler, Exception ex) {

        String uri = request.getRequestURI();
        String accept = request.getHeader("Accept");

        if (uri.indexOf("/rest/") > -1) {

            String viewName = determineViewName(ex, request);

            if (viewName != null) {

                Integer statusCode = determineStatusCode(request, viewName);
                if (statusCode != null) {
                    applyStatusCodeIfPossible(request, response, statusCode);
                }

                logger.debug("Resolving to default view change ajax : " + viewName.concat("_ajax"));

                return getModelAndView(viewName.concat("_ajax"), ex, request);

            } else {
                return null;
            }

        } else if (accept.indexOf("application/json") > -1) {

            try {

                ObjectMapper mapper = new ObjectMapper();
                EgovMap result = new EgovMap();

                result.put("message", ex.toString());
                result.put("result", "no");
                request.setAttribute("json", mapper.writeValueAsString(result));

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return getModelAndView("exceptions/commonAjaxError", ex, request);

        } else {
            return super.doResolveException(request, response, handler, ex);
        }

    }
}
