package egovframework.framework.support;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.MessageDigest;

/**************************************************************************************************************************************************************************************
 * 암호화/복호화 관련 클래스
 * 해쉬 알고리즘 : SHA, 256,512 를 지원하고 암복호화 암호화는 AES 대칭키 암호화 방식을 사용한다.
 *
 * @author YoungHo
 * @since 2016. 3. 10.
 * @see
 * Copyright(c) 2016 TASYSTEM Corporation. All rights reserved.
 *
 **************************************************************************************************************************************************************************************/
public class TApassword {

    private static final String charset = "UTF-8";

    private static final String enckeyToKey = "E2ON)(*&^%$#@!";

    public static String getSHA256(String password) {
        return getSHA("SHA-256", password);
    }

    public static String getSHA512(String password) {
        return getSHA("SHA-512", password);
    }

    private static String getSHA(String algorithm, String password) {

        StringBuffer sb = new StringBuffer();

        try {

            MessageDigest sh = MessageDigest.getInstance(algorithm);
            sh.update(password.getBytes(Charset.forName(TApassword.charset)));

            byte byteData[] = sh.digest();

            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

        } catch (Exception e) {
            return null;
        }

        return sb.toString();

    }

    public static String AESdecrypt(String password) {

        try {

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
            byte[] b = TApassword.enckeyToKey.getBytes(TApassword.charset);
            int len = b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] results = cipher.doFinal(decoder.decodeBuffer(password));
            return new String(results, TApassword.charset);

        } catch (Exception e) {
            return null;
        }

    }

    public static String AESencrypt(String password) {

        try {

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
            byte[] b = TApassword.enckeyToKey.getBytes(TApassword.charset);
            int len = b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

            byte[] results = cipher.doFinal(password.getBytes(TApassword.charset));
            BASE64Encoder encoder = new BASE64Encoder();

            return encoder.encode(results);

        } catch (Exception e) {
            return null;
        }

    }

    /**/
     public static void main(String a[]){

     String password = "1111";

     System.out.println("Original Text Password : " + password + "\n\n" );
     System.out.println("sha-256 : " + TApassword.getSHA256(password) );
     System.out.println("sha-512 : " + TApassword.getSHA512(password) );
     String enc = TApassword.AESencrypt(password);
     System.out.println("AES enc : " + enc);
     System.out.println("AES dec : " + TApassword.AESdecrypt(enc));

     }
     /**/

}