package egovframework.framework.support;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**************************************************************************************************************************************************************************************
 * 서버 사이드 공통 벨리데이션 클래스
 *
 * @author YoungHo
 * @since 2016. 2. 29.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class TAvalidator extends ValidationUtils {


    /*
     * Object 에서 해당 데이터를 원하는 데이터형으로 리턴한다.
     */
    public static int getInt(Errors errors, String field) {

        try {
            return Integer.parseInt(errors.getFieldValue(field).toString());
        } catch (Exception e) {
            return 0;
        }

    }

    /**
     * Integer 리턴
     */
    public static Integer getInteger(Errors errors, String field) {

        try {
            return Integer.getInteger(errors.getFieldValue(field).toString());
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * String 리턴
     */
    public static String getString(Errors errors, String field) {

        try {
            return errors.getFieldValue(field).toString();
        } catch (Exception e) {
            return null;
        }

    }

    /*
     * error 코드를 Global 지정
     */
    public static void reject(Errors errors, String msgKey) {

        errors.reject(msgKey);

    }

    /*
     * error 코드와 swap 문자열이 존재 할 경우
     */
    public static void reject(Errors errors, String msgKey, Object[] arguments) {

        errors.reject(msgKey, arguments, null);
    }

    /*
     * error 코드와 field 를 직접 지정
     */
    public static void rejectValue(Errors errors, String field, String msgKey) {

        errors.rejectValue(field, msgKey, null, null);

    }

    /*
     * error 코드와 field 를 직접 지정하며 swap 문자열이 존재 할 경우
     */
    public static void rejectValue(Errors errors, String field, String msgKey, Object[] arguments) {

        errors.rejectValue(field, msgKey, arguments, null);

    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * Number
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    /*
     * 숫자형 데이터인지 체크한다.
     */
    public static void rejectIsNumber(Errors errors, String field, String msgKey) {

        try {

            String value = getString(errors, field);

            Pattern p = Pattern.compile("[^\\d]");
            Matcher m = p.matcher(value);

            if (m.find()) {
                rejectValue(errors, field, msgKey);
            }

        } catch (Exception e) {

            rejectValue(errors, field, msgKey);

        }

    }

    /*
     * 데이터가 0 이거나 값이 없다면 오류
     */
    public static void rejectIsZero(Errors errors, String field, String msgKey) {

        try {

            String value = getString(errors, field);

            if (null == value) {
                rejectValue(errors, field, msgKey);
            } else if (StringUtils.isEmpty(value) || Integer.parseInt(value) == 0) {
                rejectValue(errors, field, msgKey);
            }

        } catch (Exception e) {

            rejectValue(errors, field, msgKey);

        }

    }

    /*
     * 데이터가 포함되어있으면 오류
     */
    public static void rejectIsNumberScopeIn(Errors errors, String field, String msgKey, int begin, int end) {

        rejectIsNumberScopeValidation(errors, field, msgKey, begin, end, 1);

    }

    /*
     * 데이터가 포함되어 있지 않으면 오류
     */
    public static void rejectIsNumberScopeNotIn(Errors errors, String field, String msgKey, int begin, int end) {

        rejectIsNumberScopeValidation(errors, field, msgKey, begin, end, 2);

    }

    public static void rejectIsNumberScopeValidation(Errors errors, String field, String msgKey, int begin, int end, int m) {

        try {

            int i = getInt(errors, field);

            if (i >= begin && i <= end) {

                // in
                if (m == 1) {
                    rejectValue(errors, field, msgKey);
                }

            } else {

                // not in
                if (m == 2) {
                    rejectValue(errors, field, msgKey);
                }

            }

        } catch (Exception e) {

            rejectValue(errors, field, msgKey);

        }

    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * String
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    /*
     * 문자의 길이(Length)를 체크한다.
     */

    public static void rejectLengthCheck(Errors errors, String field, String msgKey, int max) {

        try {
            String value = getString(errors, field);

            if (StringUtils.isEmpty(value)) {

                rejectValue(errors, field, msgKey);

            } else if (value.length() > max) {

                rejectValue(errors, field, msgKey);

            }

        } catch (Exception e) {

            rejectValue(errors, field, msgKey);

        }

    }

    /*
     * 지정한 문자열과 다르면 애러
     */
    public static void rejectNotEqulas(Errors errors, String field, String msgKey, String strEquals) {

        rejectEqulasValidation(errors, field, msgKey, strEquals, 1);

    }

    /*
     * 지정한 문자열과 같으면 애러
     */
    public static void rejectEqulas(Errors errors, String field, String msgKey, String strEquals) {

        rejectEqulasValidation(errors, field, msgKey, strEquals, 2);

    }

    public static void rejectEqulasValidation(Errors errors, String field, String msgKey, String strEquals, int m) {

        try {

            String value = getString(errors, field);

            if (StringUtils.isEmpty(value)) {

                rejectValue(errors, field, msgKey);

            } else {

                if (m == 1 && !value.equals(strEquals)) {

                    rejectValue(errors, field, msgKey);

                } else if (m == 2 && value.equals(strEquals)) {

                    rejectValue(errors, field, msgKey);

                }
            }

        } catch (Exception e) {

            rejectValue(errors, field, msgKey);

        }

    }

}
