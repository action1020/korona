package egovframework.framework.support;


import lombok.Getter;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by action1020s@gmail.com on 2018-02-22
 */
@Getter
public class TAExcelParser {

    private InputStream inputStream;

    @Getter
    private Boolean isSuccess = true;

    @Getter
    private Boolean isOnlyString = true;

    @Getter
    private String errorMsg = "";

    public TAExcelParser(InputStream inputStream, Boolean isOnlyString) {
        this.inputStream = inputStream;
        this.isOnlyString = isOnlyString;
    }

    public List parse(String fileName) {

        int dot = fileName.lastIndexOf(".") + 1;
        String ext = fileName.substring(dot).toLowerCase();

        if ("xls".equals(ext)) {
            return parseToXls();
        } else if ("xlsx".equals(ext)) {
            return parseToXlsx();
        } else
            throw new RuntimeException("File is Not Excel Formatted.");
    }

    private List parseToXls() {

        List list = null;

        try {

            list = new ArrayList();
            HashMap hashMap = null;

            POIFSFileSystem fs = new POIFSFileSystem(this.inputStream);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;

            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) cols = tmp;
                }
            }

            for (int r = 0; r <= rows; r++) {
                row = sheet.getRow(r);
                hashMap = new HashMap();
                if (row != null && r != 0) {

                    hashMap.put("rows", r - 1);
                    hashMap.put("cols", cols);

                    for (int c = 0; c < cols; c++) {
                        cell = row.getCell(c);

                        if (cell != null) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_STRING:
                                    hashMap.put("c" + c, cell.getStringCellValue().toString());
                                    break;
                                case Cell.CELL_TYPE_BOOLEAN:
                                    if (isOnlyString) hashMap.put("c" + c, String.valueOf(cell.getBooleanCellValue()));
                                    else hashMap.put("c" + c, cell.getBooleanCellValue());
                                    break;
                                case Cell.CELL_TYPE_NUMERIC:
                                    if (isOnlyString) {
                                        cell.setCellType(Cell.CELL_TYPE_STRING);
                                        hashMap.put("c" + c, cell.getStringCellValue());
                                    } else {
                                        hashMap.put("c" + c, cell.getNumericCellValue());
                                    }
                                    break;
                                default:
                                    hashMap.put("c" + c, cell.getStringCellValue());
                                    break;
                            }
                        } else {
                            hashMap.put("c" + c, "");
                        }
                    }
                    list.add(hashMap);
                }
            }

        } catch (Exception e) {
            isSuccess = false;
            errorMsg = e.getMessage();
            e.printStackTrace();
        }

        return list;

    }

    private List parseToXlsx() {

        List list = null;

        try {

            list = new ArrayList();
            HashMap hashMap = null;

            XSSFWorkbook workbook = new XSSFWorkbook(this.inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            int rows = 0;
            int cols = 0;

            while (rowIterator.hasNext()) {

                cols = 0;
                Row row = rowIterator.next();

                if (rows == 0) {
                    rows++;
                    continue;
                }

                Iterator<Cell> cellIterator = row.cellIterator();

                hashMap = new HashMap();
                hashMap.put("rows", rows);
                hashMap.put("cols", cols);

                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            hashMap.put("c" + cols, (String) cell.getStringCellValue().toString());
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            if (isOnlyString) hashMap.put("c" + cols, String.valueOf(cell.getBooleanCellValue()));
                            else hashMap.put("c" + cols, cell.getBooleanCellValue());
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            if (isOnlyString) hashMap.put("c" + cols, String.valueOf((int) cell.getNumericCellValue()));
                            else hashMap.put("c" + cols, cell.getNumericCellValue());
                            break;
                        default:
                            hashMap.put("c" + cols, cell.getStringCellValue());
                            break;
                    }

                    cols++;
                }


                list.add(hashMap);
                rows++;
            }

        } catch (Exception e) {
            isSuccess = false;
            errorMsg = e.getMessage();
            e.printStackTrace();
        }

        return list;

    }
}