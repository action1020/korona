package egovframework.framework.support;

/*************************************************************************************************************************************************************************************
 * 유틸리티 클래스
 *
 * @author YoungHo
 * @since 2016. 3. 17.
 * @see
 *
 **************************************************************************************************************************************************************************************/
public class TAUtils {

    private static final String isEmptyString = "";

    /**
     * Object가 null 이면 빈문자열을 반환한다.
     *
     * @param obj
     * @return
     */
    public static Object ObjectNullToEmptyString(Object obj) {
        return ObjectNullToString(obj, isEmptyString);
    }

    /**
     * Object가 null 이면 defense 문자열을 반환한다.
     *
     * @param obj
     * @param defense
     * @return
     */
    public static Object ObjectNullToString(Object obj, String defense) {
        return obj == null ? defense : obj;
    }


}
