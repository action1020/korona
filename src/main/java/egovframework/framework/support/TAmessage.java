package egovframework.framework.support;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**************************************************************************************************************************************************************************************
 * 메시지 처리 클래스
 *
 * @author YoungHo
 * @since 2016. 2. 29.
 * @see
 *
 **************************************************************************************************************************************************************************************/
@Component
public class TAmessage implements MessageSourceAware {

    private static MessageSource messageSource;

    public static String getMessage(String key) {
        return messageSource.getMessage(key, null, Locale.getDefault());
    }

    public static String getMessage(String key, Locale locale) {
        return messageSource.getMessage(key, null, locale);
    }

    public static String getMessage(String key, Object[] arguments) {
        return getMessage(key, arguments, Locale.getDefault());
    }

    public static String getMessage(String key, Object[] arguments, Locale locale) {
        return messageSource.getMessage(key, arguments, locale);
    }

    @SuppressWarnings("static-access")
    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}